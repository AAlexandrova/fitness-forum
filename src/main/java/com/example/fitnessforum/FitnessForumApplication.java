package com.example.fitnessforum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FitnessForumApplication {

    public static void main(String[] args) {
        SpringApplication.run(FitnessForumApplication.class, args);
    }

}

