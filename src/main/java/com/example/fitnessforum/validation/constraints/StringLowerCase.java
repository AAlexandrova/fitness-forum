package com.example.fitnessforum.validation.constraints;

//custom validation that the String is in lower case
import com.example.fitnessforum.validation.validator.StringLowerCaseValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;

@Documented
@Target({ FIELD, PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = StringLowerCaseValidator.class)
public @interface StringLowerCase {
    String message() default "String not in lowercase.";
    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
