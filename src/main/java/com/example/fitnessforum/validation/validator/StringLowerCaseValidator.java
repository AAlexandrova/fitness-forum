package com.example.fitnessforum.validation.validator;

import com.example.fitnessforum.validation.constraints.StringLowerCase;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static java.lang.Character.isLowerCase;

public class StringLowerCaseValidator implements ConstraintValidator<StringLowerCase, String> {
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if(value == null){
            return true;
        }
        char[] chars = value.toCharArray();
        Character[] characters = new Character[chars.length];

        for (int i = 0; i < chars.length; i++) {
            characters[i] = chars[i];
        }
        boolean isLowerCase = true;
        for(Character c : characters){
            if(!(isLowerCase(c) || c.equals(' '))){
                isLowerCase = false;
            }
        }
        return isLowerCase;
    }

    @Override
    public void initialize(StringLowerCase constraintAnnotation){
        ConstraintValidator.super.initialize(constraintAnnotation);
    }
}
