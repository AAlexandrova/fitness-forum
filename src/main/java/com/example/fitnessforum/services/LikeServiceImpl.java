package com.example.fitnessforum.services;

import com.example.fitnessforum.exceptions.AuthorizationException;
import com.example.fitnessforum.exceptions.CustomException;
import com.example.fitnessforum.exceptions.EntityNotFoundException;
import com.example.fitnessforum.models.Like;
import com.example.fitnessforum.models.Post;
import com.example.fitnessforum.models.User;
import com.example.fitnessforum.repositories.contracts.LikeRepository;
import com.example.fitnessforum.services.contracts.LikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LikeServiceImpl implements LikeService {
    private final LikeRepository likeRepository;

    @Autowired
    public LikeServiceImpl(LikeRepository likeRepository) {
        this.likeRepository = likeRepository;
    }

    @Override
    public List<Like> getLikesByPostId(int postId) {
        return likeRepository.getLikesByPostId(postId);
    }

    @Override
    public List<Like> getLikes() {
        return likeRepository.getLikes();
    }

    @Override
    public Like getLikeById(int likeId, User user) {
        checkAdmin(user);
        return likeRepository.getLikeById(likeId);
    }

    @Override
    public void createLike(Like like) {
        checkBlocked(like.getUser());
        //не може един и същ юзър да добавя лайк на пост, който вече е харесал.
        int currentUserIdToLike = like.getUser().getUserId();
        int currentPostIdToLike = like.getPost().getPostId();
        if(userLikesPost(currentPostIdToLike, currentUserIdToLike)){
            throw new CustomException(String.format("User with userId %d already reacted to post with postId %d",
                    currentUserIdToLike, currentPostIdToLike));
        }
        //не може юзър да хареса свой собствен пост
        if (like.getUser().equals(like.getPost().getUser())) {
            throw new CustomException("A user cannot like his/her own posts.");
        }
        likeRepository.createLike(like);
    }

    @Override
    public boolean userLikesPost(int postId, int userId){
        boolean userLikesPost = true;
        // Ако този юзър вече е харесал този пост,
        // от репото ще ни се върне лайк. Ако не - ще се хвърли грешка
        try{
            Like likeByThisUserForThisPost = likeRepository
                    .getLikeByPostAndUser(postId, userId);}
        catch (EntityNotFoundException e){
            userLikesPost = false;
        }
        return userLikesPost;
    }

    @Override
    public void deleteLike(int likeId) {
        Like likeToDelete = likeRepository.getLikeById(likeId);
        checkBlocked(likeToDelete.getUser());
        likeRepository.deleteLike(likeId);

    }

    @Override
    public Like getLikeByPostAndUser(Post post, User user){
        return likeRepository.getLikeByPostAndUser(post.getPostId(), user.getUserId());
    }

    @Override
    public long getLikesCount(int postId) {
        return likeRepository.getLikesCount(postId);
    }

    @Override
    public long getDislikesCount(int postId) {
        return likeRepository.getDislikesCount(postId);
    }

    private void checkBlocked(User user){
        if(user.isBlocked()){
            throw new AuthorizationException("Blocked users cannot like/dislike posts.");
        }
    }

    private void checkAdmin(User user){
        if(!user.isAdmin()){
            throw new AuthorizationException("Only admins can see administrative like data.");
        }
    }
}


