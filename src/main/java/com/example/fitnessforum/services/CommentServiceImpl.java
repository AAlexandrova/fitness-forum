package com.example.fitnessforum.services;

import com.example.fitnessforum.exceptions.AuthorizationException;
import com.example.fitnessforum.models.Comment;
import com.example.fitnessforum.models.CommentFilterOptions;
import com.example.fitnessforum.models.Post;
import com.example.fitnessforum.models.User;
import com.example.fitnessforum.repositories.contracts.CommentRepository;
import com.example.fitnessforum.repositories.contracts.PostRepository;
import com.example.fitnessforum.services.contracts.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CommentServiceImpl implements CommentService {
    private final CommentRepository commentRepository;
    private final PostRepository postRepository;

    @Autowired
    public CommentServiceImpl(CommentRepository commentRepository, PostRepository postRepository) {
        this.commentRepository = commentRepository;
        this.postRepository = postRepository;
    }

    @Override
    public List<Comment> getComments(User user) {
        return commentRepository.getComments();
    }

    @Override
    public Comment getCommentById(int commentId, User user) {
        return commentRepository.getCommentById(commentId);
    }

    @Override
    public Comment getCommentById(int commentId, int postId) {
        return commentRepository.getCommentById(commentId, postId);
    }

    @Override
    public List<Comment> getCommentByPostId(int postId) {
        return commentRepository.getCommentByPostId(postId);
    }

    @Override
    public List<Comment> getCommentByPostId(int postId, CommentFilterOptions commentFilterOptions) {
        return commentRepository.getCommentByPostId(postId, commentFilterOptions);
    }

    @Override
    public Comment createComment(Comment comment, User user) {
        checkBlocked(user);
        commentRepository.createComment(comment);
        return comment;
    }

    @Override
    public void updateComment(Comment comment, User user, Post post) {
        checkBlocked(user);
        checkAuthorization(comment, user);
        commentRepository.updateComment(comment);
    }
    @Override
    public void updateComment(Comment comment, User user) {
        checkBlocked(user);
        checkAuthorization(comment, user);
        commentRepository.updateComment(comment);
    }

    @Override
    public void deleteComment(int commentId, User user, Post post) {
        checkBlocked(user);
        checkAuthorization(commentRepository.getCommentById(commentId), user);
        commentRepository.deleteComment(commentId);
    }
    @Override
    public void deleteComment(int commentId, User user) {
        checkBlocked(user);
        checkAuthorization(commentRepository.getCommentById(commentId), user);
        commentRepository.deleteComment(commentId);
    }

    @Override
    public List<Comment> filter(Integer postId, Optional<Integer> commentId,
                                Optional<Integer> userId, Optional<String> sort) {
        return commentRepository.filter(postId, commentId, userId, sort);
    }

    private void checkAuthorization(Comment comment, User user) {
        if (!(user.isAdmin() || comment.getUser().equals(user))) {
            throw new AuthorizationException("Only admins and creator can modify comments.");
        }
    }

    private void checkBlocked(User user) {
        if (user.isBlocked()) {
            throw new AuthorizationException("Blocked users cannot modify comments.");
        }
    }

    private void checkAdmin(User user) {
        if (!user.isAdmin()) {
            throw new AuthorizationException("You are not admin!");
        }
    }
}
