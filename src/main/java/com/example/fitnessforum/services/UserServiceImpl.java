package com.example.fitnessforum.services;

import com.example.fitnessforum.exceptions.AuthorizationException;
import com.example.fitnessforum.exceptions.EntityDuplicateException;
import com.example.fitnessforum.exceptions.EntityNotFoundException;
import com.example.fitnessforum.models.User;
import com.example.fitnessforum.models.UserFilterOptions;
import com.example.fitnessforum.repositories.contracts.UserRepository;
import com.example.fitnessforum.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getUsers() {
        return userRepository.getUsers();
    }

    @Override
    public List<User> getUsers(UserFilterOptions userFilterOptions) {
        return userRepository.getUsers(userFilterOptions);
    }

    @Override
    public Long getUsersCount() {
        return userRepository.getUsersCount();
    }

    @Override
    public List<User> getUsersWithPosts() { return userRepository.getUsersWithPosts(); }

    @Override
    public User getUserById(int userId) {
        return userRepository.getUserById(userId);
    }

    @Override
    public User getUserByUsername(String username) {
        return userRepository.getUserByUsername(username);
    }

    @Override
    public List<User> filter(Optional<String> firstName, Optional<String> lastName, Optional<String> email,
                             Optional<Boolean> isAdmin, Optional<Boolean> isBlocked, Optional<String> username,
                             Optional<String> sort) {
        return userRepository.filter(firstName, lastName, email, isAdmin,
                isBlocked, username, sort);
    }

    @Override
    public User createUser(User user) {
        //да се уверим, че няма два юзъра с един и същ username или email
        boolean duplicateUsernameExists = true;
        try {
            userRepository.getUserByUsername(user.getUsername());
        } catch (EntityNotFoundException e) {
            duplicateUsernameExists = false;
        }
        boolean duplicateEmailExists = true;
        try {
            userRepository.getUserByEmail(user.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateEmailExists = false;
        }

        if (duplicateUsernameExists) {
            throw new EntityDuplicateException("User", "username", user.getUsername());
        }
        if (duplicateEmailExists) {
            throw new EntityDuplicateException("User", "email", user.getEmail());
        }
        userRepository.createUser(user);
        return user;
    }

    @Override
    public void updateUser(User user, User authentication) {
        checkBlocked(authentication);
        checkAuthorization(user, authentication);
        boolean duplicateUsernameExists = true;
        try {
            User existingUser = userRepository.getUserByUsername(user.getUsername());
            if (existingUser.getUserId() == user.getUserId()) {
                duplicateUsernameExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateUsernameExists = false;
        }

        if (duplicateUsernameExists) {
            throw new EntityDuplicateException("User", "username", user.getUsername());
        }

        boolean duplicateEmailExists = true;
        try {
            User existingUser = userRepository.getUserByEmail(user.getEmail());
            if (existingUser.getUserId() == user.getUserId()) {
                duplicateEmailExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateEmailExists = false;
        }

        if (duplicateEmailExists) {
            throw new EntityDuplicateException("User", "email", user.getEmail());
        }
        userRepository.updateUser(user);
    }

    @Override
    public void deleteUser(int userId, User authentication) {
        User user = userRepository.getUserById(userId);
        checkAuthorization(user, authentication);
        checkBlocked(authentication);
        user.setFirstName("unknown");
        user.setLastName("unknown");
        user.setUsername("unknown");
        user.setPassword("unknown");
        user.setAdmin(false);
        user.setBlocked(true);

        userRepository.updateUser(user);
    }

    private void checkAuthorization(User user, User authentication) {
        if (!(authentication.isAdmin() || user.getUserId() == authentication.getUserId())) {
            throw new AuthorizationException("Only admin or the same user can modify a particular user.");
        }
    }

    private void checkBlocked(User user) {
        if (user.isBlocked()) {
            throw new AuthorizationException("Blocked users cannot update profiles.");
        }
    }
}
