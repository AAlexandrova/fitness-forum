package com.example.fitnessforum.services;

import com.example.fitnessforum.exceptions.AuthorizationException;
import com.example.fitnessforum.exceptions.EntityDuplicateException;
import com.example.fitnessforum.exceptions.EntityNotFoundException;
import com.example.fitnessforum.models.Post;
import com.example.fitnessforum.models.Tag;
import com.example.fitnessforum.models.User;
import com.example.fitnessforum.repositories.contracts.PostRepository;
import com.example.fitnessforum.repositories.contracts.TagRepository;
import com.example.fitnessforum.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TagServiceImpl implements TagService {
    private final TagRepository tagRepository;
    private final PostRepository postRepository;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository, PostRepository postRepository) {
        this.tagRepository = tagRepository;
        this.postRepository = postRepository;
    }

    @Override
    public List<Tag> getTags(User user) {
        checkAdmin(user);
        return tagRepository.getTags();
    }

    @Override
    public Tag getTagById(int tagId) {
        return tagRepository.getTagById(tagId);
    }

    @Override
    public Tag getTagById(User user, int tagId) {
        checkAdmin(user);
        return tagRepository.getTagById(tagId);
    }

    @Override
    public Tag getOrCreateTag(User user, Tag tag) {
        checkBlocked(user);
        //ако вече има таг с този title, не може да се създаде нов
        boolean tagExists = true;
        try {
            tagRepository.getTagByTitle(tag.getTitle());
        } catch (EntityNotFoundException e) {
            tagExists = false;
        }

        if (!tagExists) {
            tagRepository.createTag(tag);
        }
        return tagRepository.getTagByTitle(tag.getTitle());
    }

    @Override
    public void updateTag(User user, Tag tag) {
        checkBlocked(user);
        checkAdmin(user);
        //не може да има два тага с един и същ title
        boolean duplicateExists = true;
        try {
            tagRepository.getTagByTitle(tag.getTitle());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new EntityDuplicateException("Tag", "title", tag.getTitle());
        }
        tagRepository.updateTag(tag);
    }

    @Override
    public void updateTagOnPost(User user, Post post, int oldTagId, Tag newTag) {
        //Автентикацията се проверява в методите deleteTagFromPost и AddTagToPost
        //премахваме стария таг
        deleteTagFromPost(user, post, oldTagId);

        //Ако този пост вече има новия таг:
        //Метод addTagToPost ще ни хвърли грешка
        Tag repositoryTag = getOrCreateTag(user, newTag);
        addTagToPost(user, post, repositoryTag);
    }

    @Override
    public void deleteTag(User user, int tagId) {
        checkBlocked(user);
        checkAdmin(user);
        //Първо трябва да махнем този таг от всички постове, които го имат.
        Tag tagToDelete = tagRepository.getTagById(tagId);
        List<Post> posts = postRepository.getPosts().stream()
                .filter(post -> post.getTags().contains(tagToDelete))
                .collect(Collectors.toList());
        for (Post post : posts) {
            post.getTags().remove(tagToDelete);
            postRepository.updatePost(post);
        }
        tagRepository.deleteTag(tagId);
    }

    @Override
    public void addTagToPost(User user, Post post, Tag tag) {
        checkBlocked(user);
        //само създателят на поста или админ могат да добавят таг към този пост
        checkAuthorization(user, post);
        //няма смисъл да хвърляме грешка, ако този таг вече е добавен към поста.
        //ако този пост вече има този таг, няма да се добави в сета от тагове.
        post.getTags().add(tag);
        postRepository.updatePost(post);
    }


    @Override
    public void deleteTagFromPost(User user, Post post, int tagId) {
        checkBlocked(user);
        checkAuthorization(user, post);
        Tag tagToDelete = getTagById(tagId);
        if (!post.getTags().contains(tagToDelete)) {
            throw new EntityNotFoundException(String.format("Post with postId %d does not have a tag '%s'."
                    , post.getPostId(), tagToDelete.getTitle()));
        }
        post.getTags().remove(tagToDelete);
        postRepository.updatePost(post);
    }

    private void checkAuthorization(User user, Post post) {
        if (!(user.isAdmin() || post.getUser().equals(user))) {
            throw new AuthorizationException("Only admin or creator of a post can modify its tags.");
        }
    }

    private void checkBlocked(User user) {
        if (user.isBlocked()) {
            throw new AuthorizationException("Blocked users cannot modify tags.");
        }
    }

    private void checkAdmin(User user) {
        if (!user.isAdmin()) {
            throw new AuthorizationException("Only admins can access tags.");
        }
    }
}



