package com.example.fitnessforum.services;

import com.example.fitnessforum.exceptions.AuthorizationException;
import com.example.fitnessforum.exceptions.CustomException;
import com.example.fitnessforum.exceptions.EntityDuplicateException;
import com.example.fitnessforum.exceptions.EntityNotFoundException;
import com.example.fitnessforum.models.Phone;
import com.example.fitnessforum.models.User;
import com.example.fitnessforum.repositories.contracts.PhoneRepository;
import com.example.fitnessforum.services.contracts.PhoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PhoneServiceImpl implements PhoneService {
    private final PhoneRepository phoneRepository;

    @Autowired
    public PhoneServiceImpl(PhoneRepository phoneRepository) {
        this.phoneRepository = phoneRepository;
    }

    @Override
    public List<Phone> getPhones() {
        return phoneRepository.getPhones();
    }

    @Override
    public Phone getPhoneById(int phoneId) {
        return phoneRepository.getPhoneById(phoneId);
    }

    @Override
    public Phone getPhoneByUserId(int userId) {
        return phoneRepository.getPhoneByUserId(userId);
    }

    @Override
    public List<Phone> filter(Optional<String> phone, Optional<Integer> userId, Optional<String> sort) {
        return phoneRepository.filter(phone, userId, sort);
    }

    @Override
    public Phone createPhone(Phone phone, User user) {
        checkBlocked(user);
        checkAuthorization(user);
        boolean duplicateExists = true;

        if (!phone.getUser().isAdmin()) {
            throw new CustomException("Phone can have only admins!");
        }

        try {
            phoneRepository.getPhoneById(phone.getPhoneId());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new EntityDuplicateException("Phone", phone.getPhoneId());
        }
        phoneRepository.createPhone(phone);
        return phone;
    }

    @Override
    public void updatePhone(Phone phone, User user) {
        checkBlocked(user);
        checkAuthorization(user);
        boolean duplicateExists = true;

        try {
            Phone existingPhone = phoneRepository.getPhoneById(phone.getPhoneId());
            if (existingPhone.getPhoneId() == phone.getPhoneId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (!phone.getUser().isAdmin()) {
            throw new CustomException("Phone can have only admins!");
        }

        if (duplicateExists) {
            throw new EntityDuplicateException("Phone", "number", phone.getPhone());
        }
        phoneRepository.updatePhone(phone);
    }

    @Override
    public void deletePhone(int phoneId, User user) {
        checkBlocked(user);
        checkAuthorization(user);
        phoneRepository.deletePhone(phoneId);
    }

    private void checkAuthorization(User user) {
        if (!user.isAdmin()) {
            throw new AuthorizationException("Only admins can modify phones.");
        }
    }

    private void checkBlocked(User user) {
        if (user.isBlocked()) {
            throw new AuthorizationException("Blocked users cannot update phones.");
        }
    }
}
