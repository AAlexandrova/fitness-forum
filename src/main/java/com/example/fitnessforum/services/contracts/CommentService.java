package com.example.fitnessforum.services.contracts;

import com.example.fitnessforum.models.Comment;
import com.example.fitnessforum.models.CommentFilterOptions;
import com.example.fitnessforum.models.Post;
import com.example.fitnessforum.models.User;

import java.util.List;
import java.util.Optional;

public interface CommentService {
    List<Comment> getComments(User user);

    Comment getCommentById(int commentId, User user);

    Comment getCommentById(int commentId, int postId);

    List<Comment> getCommentByPostId(int postId);
    List<Comment> getCommentByPostId(int postId, CommentFilterOptions commentFilterOptions);

    Comment createComment(Comment comment, User user);

    void updateComment(Comment comment, User user, Post post);
    void updateComment(Comment comment, User user);

    void deleteComment(int commentId, User user, Post post);

    void deleteComment(int commentId, User user);

    List<Comment> filter(Integer postId, Optional<Integer> commentId, Optional<Integer> userId, Optional<String> sort);
}
