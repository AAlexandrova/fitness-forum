package com.example.fitnessforum.services.contracts;

import com.example.fitnessforum.models.User;
import com.example.fitnessforum.models.UserFilterOptions;

import java.util.List;
import java.util.Optional;

public interface UserService {

    List<User> getUsers();

    List<User> getUsers(UserFilterOptions userFilterOptions);

    List<User> getUsersWithPosts();

    User getUserById(int id);

    User getUserByUsername(String username);

    User createUser(User user);

    void updateUser(User user, User authentication);

    void deleteUser(int userId, User authentication);

    Long getUsersCount();

    List<User> filter(Optional<String> firstName, Optional<String> lastName, Optional<String> email,
                      Optional<Boolean> isAdmin, Optional<Boolean> isBlocked, Optional<String> username,
                      Optional<String> sort);

}
