package com.example.fitnessforum.services.contracts;

import com.example.fitnessforum.models.Phone;
import com.example.fitnessforum.models.User;

import java.util.List;
import java.util.Optional;

public interface PhoneService {
    List<Phone> getPhones();

    Phone getPhoneById(int phoneId);

    Phone getPhoneByUserId(int userId);

    Phone createPhone(Phone phone, User user);

    void updatePhone(Phone phone, User user);

    void deletePhone(int phoneId, User user);

    List<Phone> filter(Optional<String> phone, Optional<Integer> userId, Optional<String> sort);
}
