package com.example.fitnessforum.services.contracts;

import com.example.fitnessforum.models.Post;
import com.example.fitnessforum.models.Tag;
import com.example.fitnessforum.models.User;

import java.util.List;

public interface TagService {
    List<Tag> getTags(User user);

    Tag getOrCreateTag(User user, Tag tag);

    Tag getTagById(int tagId);

    Tag getTagById(User user, int tagId);

    void updateTag(User user, Tag tag);

    void updateTagOnPost(User user, Post post, int oldTagId, Tag newTag);

    void deleteTag(User user, int tagId);

    void addTagToPost(User user, Post post, Tag tag);

    void deleteTagFromPost(User user, Post post, int tagId);
}
