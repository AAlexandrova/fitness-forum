package com.example.fitnessforum.services.contracts;

import com.example.fitnessforum.models.Like;
import com.example.fitnessforum.models.Post;
import com.example.fitnessforum.models.User;

import java.util.List;

public interface LikeService {
    List<Like> getLikesByPostId(int postId);

    Like getLikeById(int likeId, User user);

    List<Like> getLikes();
    void createLike(Like like);

    void deleteLike(int likeId);

    Like getLikeByPostAndUser(Post post, User user);

    boolean userLikesPost(int postId, int userId);

    long getLikesCount(int postId);

    long getDislikesCount (int postId);
}
