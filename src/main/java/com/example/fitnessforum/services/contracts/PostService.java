package com.example.fitnessforum.services.contracts;

import com.example.fitnessforum.models.Post;
import com.example.fitnessforum.models.PostFilterOptions;
import com.example.fitnessforum.models.User;

import java.util.List;
import java.util.Optional;

public interface PostService {
    List<Post> getPosts(PostFilterOptions filterOptions);
    Post getPostById(int id);

    void createPost(Post post, User user);

    void updatePost(Post post, User user);

    void deletePost(int id, User user);

    List<Post> getMostLikedPosts();

    List<Post> getTenMostCommentedPosts();

    List<Post> getTenMostRecentPosts();

    Long getPostsCount();
}
