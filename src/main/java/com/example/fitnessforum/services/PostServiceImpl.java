package com.example.fitnessforum.services;


import com.example.fitnessforum.exceptions.AuthorizationException;
import com.example.fitnessforum.exceptions.EntityDuplicateException;
import com.example.fitnessforum.exceptions.EntityNotFoundException;
import com.example.fitnessforum.models.*;
import com.example.fitnessforum.repositories.contracts.CommentRepository;
import com.example.fitnessforum.repositories.contracts.LikeRepository;
import com.example.fitnessforum.repositories.contracts.PostRepository;
import com.example.fitnessforum.services.contracts.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImpl implements PostService {
    private final PostRepository postRepository;
    private final LikeRepository likeRepository;
    private final CommentRepository commentRepository;

    @Autowired
    public PostServiceImpl(PostRepository postRepository, LikeRepository likeRepository, CommentRepository commentRepository) {
        this.postRepository = postRepository;
        this.likeRepository = likeRepository;
        this.commentRepository = commentRepository;
    }

    @Override
    public List<Post> getPosts(PostFilterOptions filterOptions) {
        return postRepository.getPosts(filterOptions);
    }

    @Override
    public List<Post> getMostLikedPosts() {
        return postRepository.getMostLikedPosts();
    }

    @Override
    public List<Post> getTenMostCommentedPosts() {
        return postRepository.getTenMostCommentedPosts();
    }

    @Override
    public List<Post> getTenMostRecentPosts() {
        return postRepository.getTenMostRecentPosts();
    }

    @Override
    public Long getPostsCount() {
        return postRepository.getPostsCount();
    }

    @Override
    public Post getPostById(int postId) {
        return postRepository.getPostById(postId);
    }

    @Override
    public void createPost(Post post, User user) {
        checkBlocked(user);
        //не може да има два поста с един и същ title
        boolean postExists = true;
        try {
            postRepository.getPostByTitle(post.getTitle());
        } catch (EntityNotFoundException e) {
            postExists = false;
        }

        if (postExists) {
            throw new EntityDuplicateException("Post", "title", post.getTitle());
        }
        post.setUser(user);
        postRepository.createPost(post);
    }

    @Override
    public void updatePost(Post post, User user) {
        checkBlocked(user);
        checkAuthorization(post.getPostId(), user);
        //не може да има два поста с един и същ title
        boolean duplicateExists = true;
        try {
            //ако подаваме вече съществуващ title, но и същото ид,
            // тогава ъпдейтваме някое друго поле на поста и е ок.
            Post existingPost = postRepository.getPostByTitle(post.getTitle());
            if (existingPost.getPostId() == post.getPostId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new EntityDuplicateException("Post", "title", post.getTitle());
        }
        postRepository.updatePost(post);
    }

    @Override
    public void deletePost(int postId, User user) {
        checkBlocked(user);
        checkAuthorization(postId, user);
        //за да изтрием някой пост, трябва първо да махнем неговите лайкове:
        boolean postHasLikes = true;
        try {
            List<Like> likes = likeRepository.getLikesByPostId(postId);
            for (Like like : likes) {
                likeRepository.deleteLike(like.getLikeId());
            }
        } catch (EntityNotFoundException e) {
            postHasLikes = false;
        }

        //за да изтрием пост, трябва първо да махнем неговите коментари
        boolean postHasComments = true;
        try{
        List<Comment> comments = commentRepository.getCommentByPostId(postId);
        for (Comment comment : comments) {
            commentRepository.deleteComment(comment.getCommentId());
        }}
        catch (EntityNotFoundException e){
            postHasComments = false;
        }
        //за да изтрием пост, трябва да махнем неговите тагове
        //за това не трябва да правим нещо конкретно, защото няма да изтриваме тага от репото
        postRepository.deletePost(postId);
    }


    private void checkAuthorization(int postId, User user) {
        Post existingPost = postRepository.getPostById(postId);
        if (!(user.isAdmin() || existingPost.getUser().equals(user))) {
            throw new AuthorizationException("Only admin or creator of a post can modify it.");
        }
    }

    private void checkBlocked(User user) {
        if (user.isBlocked()) {
            throw new AuthorizationException("Blocked users cannot create or modify posts.");
        }
    }
}
