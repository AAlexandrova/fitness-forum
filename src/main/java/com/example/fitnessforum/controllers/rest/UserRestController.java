package com.example.fitnessforum.controllers.rest;

import com.example.fitnessforum.exceptions.AuthorizationException;
import com.example.fitnessforum.exceptions.EntityDuplicateException;
import com.example.fitnessforum.exceptions.EntityNotFoundException;
import com.example.fitnessforum.helpers.AuthenticationHelper;
import com.example.fitnessforum.helpers.PhoneMapper;
import com.example.fitnessforum.helpers.UserMapper;
import com.example.fitnessforum.models.User;
import com.example.fitnessforum.models.dto.PhoneDtoOut;
import com.example.fitnessforum.models.dto.UserDto;
import com.example.fitnessforum.services.contracts.PhoneService;
import com.example.fitnessforum.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserRestController {
    private final UserService userService;
    private final UserMapper userMapper;
    private final PhoneService phoneService;
    private final PhoneMapper phoneMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserRestController(UserService userService, UserMapper userMapper, PhoneService phoneService,
                              PhoneMapper phoneMapper, AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.phoneService = phoneService;
        this.phoneMapper = phoneMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<User> getUsers() {
        return userService.getUsers();
    }

    @GetMapping("/{userId}")
    public User getUserById(@PathVariable int userId) {
        try {
            return userService.getUserById(userId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<User> filter(
            @RequestParam(name = "firstName", required = false) Optional<String> firstName,
            @RequestParam(name = "lastName", required = false) Optional<String> lastName,
            @RequestParam(name = "email", required = false) Optional<String> email,
            @RequestParam(name = "isAdmin", required = false) Optional<Boolean> isAdmin,
            @RequestParam(name = "isBlocked", required = false) Optional<Boolean> isBlocked,
            @RequestParam(name = "username", required = false) Optional<String> username,
            @RequestParam(name = "sort", required = false) Optional<String> sort
    ) {
        try {
            return userService.filter(firstName, lastName, email, isAdmin, isBlocked, username, sort);
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping
    public User createUser(@Valid @RequestBody UserDto userDto) {
        try {
            User user = userMapper.dtoToObject(userDto);
            userService.createUser(user);
            return user;
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{userId}")
    public User updateUser(@RequestHeader HttpHeaders headers,
                           @PathVariable int userId,
                           @Valid @RequestBody UserDto userDto) {
        try {
            User authentication = authenticationHelper.tryGetUser(headers);
            User existingUser = userService.getUserById(userId);
            User user;
            if (authentication.isAdmin()) {
                user = userMapper.dtoToObjectAsAdmin(existingUser, userDto);
            } else {
                user = userMapper.dtoToObjectAsUser(userId, userDto);
            }
            userService.updateUser(user, authentication);
            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{userId}")
    public void deleteUser(@RequestHeader HttpHeaders headers, @PathVariable int userId) {
        try {
            User authentication = authenticationHelper.tryGetUser(headers);
            userService.deleteUser(userId, authentication);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{userId}/phone")
    public PhoneDtoOut getPhones(@PathVariable int userId) {
        try {
            return phoneMapper.phoneDtoOut(phoneService.getPhoneByUserId(userId), new PhoneDtoOut());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
