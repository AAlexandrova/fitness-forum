package com.example.fitnessforum.controllers.rest;

import com.example.fitnessforum.exceptions.AuthorizationException;
import com.example.fitnessforum.exceptions.EntityNotFoundException;
import com.example.fitnessforum.helpers.AuthenticationHelper;
import com.example.fitnessforum.helpers.LikeMapper;
import com.example.fitnessforum.models.Like;
import com.example.fitnessforum.models.User;
import com.example.fitnessforum.services.contracts.LikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/likes")
public class LikeRestController {
    private final LikeService service;
    private final LikeMapper likeMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public LikeRestController(LikeService service, LikeMapper likeMapper, AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.likeMapper = likeMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/{likeId}")
    public Like getLikeById(@RequestHeader HttpHeaders headers, @PathVariable int likeId){
        try{
            User user = authenticationHelper.tryGetUser(headers);
            return service.getLikeById(likeId, user);
        }
        catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
