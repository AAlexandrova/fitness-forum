package com.example.fitnessforum.controllers.rest;

import com.example.fitnessforum.exceptions.AuthorizationException;
import com.example.fitnessforum.exceptions.EntityDuplicateException;
import com.example.fitnessforum.exceptions.EntityNotFoundException;
import com.example.fitnessforum.helpers.AuthenticationHelper;
import com.example.fitnessforum.helpers.TagMapper;
import com.example.fitnessforum.models.Tag;
import com.example.fitnessforum.models.User;
import com.example.fitnessforum.models.dto.TagDto;
import com.example.fitnessforum.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/tags")
public class TagRestController {
    private final TagService service;
    private final AuthenticationHelper authenticationHelper;
    private final TagMapper tagMapper;

    @Autowired
    public TagRestController(TagService service, AuthenticationHelper authenticationHelper, TagMapper tagMapper) {
        this.service = service;
        this.authenticationHelper = authenticationHelper;
        this.tagMapper = tagMapper;
    }

    @GetMapping
    public List<Tag> getAllTags(@RequestHeader HttpHeaders headers) {
        try{User user = authenticationHelper.tryGetUser(headers);
        return service.getTags(user);}
        catch (AuthorizationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{tagId}")
    public Tag getTagById(@RequestHeader HttpHeaders headers, @PathVariable int tagId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return service.getTagById(user, tagId);
        } catch (AuthorizationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
        catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{tagId}")
    public Tag updateTag(@RequestHeader HttpHeaders headers
            , @PathVariable int tagId, @Valid @RequestBody TagDto tagDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Tag tag = tagMapper.dtoToTag(tagId, tagDto);
            service.updateTag(user, tag);
            return tag;
        } catch (AuthorizationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
        catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{tagId}")
    public void deleteTag(@RequestHeader HttpHeaders headers, @PathVariable int tagId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            service.deleteTag(user, tagId);
        } catch (AuthorizationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
        catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


}
