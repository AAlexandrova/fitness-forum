package com.example.fitnessforum.controllers.rest;

import com.example.fitnessforum.exceptions.AuthorizationException;
import com.example.fitnessforum.exceptions.CustomException;
import com.example.fitnessforum.exceptions.EntityDuplicateException;
import com.example.fitnessforum.exceptions.EntityNotFoundException;
import com.example.fitnessforum.helpers.AuthenticationHelper;
import com.example.fitnessforum.helpers.PhoneMapper;
import com.example.fitnessforum.models.Phone;
import com.example.fitnessforum.models.dto.PhoneDto;
import com.example.fitnessforum.models.User;
import com.example.fitnessforum.models.dto.PhoneDtoOut;
import com.example.fitnessforum.services.contracts.PhoneService;
import com.example.fitnessforum.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/phones")
public class PhoneRestController {
    private final PhoneService phoneService;
    private final PhoneMapper phoneMapper;
    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;

    @Autowired
    public PhoneRestController(PhoneService phoneService,
                               PhoneMapper phoneMapper,
                               AuthenticationHelper authenticationHelper,
                               UserService userService) {
        this.phoneService = phoneService;
        this.phoneMapper = phoneMapper;
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
    }

    @GetMapping
    public List<PhoneDtoOut> getPhones() {
        List<Phone> phones = phoneService.getPhones();
        List<PhoneDtoOut> phoneDtoOut = new ArrayList<>();
        for (Phone phone : phones) {
            phoneDtoOut.add(phoneMapper.phoneDtoOut(phone, new PhoneDtoOut()));
        }
        return phoneDtoOut;
    }

    @GetMapping("/{phoneId}")
    public PhoneDtoOut getPhoneById(@PathVariable int phoneId) {
        try {
            return phoneMapper.phoneDtoOut(phoneService.getPhoneById(phoneId), new PhoneDtoOut());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<PhoneDtoOut> filter(
            @RequestParam(name = "phone", required = false) Optional<String> phone,
            @RequestParam(name = "userId", required = false) Optional<Integer> userId,
            @RequestParam(name = "sort", required = false) Optional<String> sort
    ) {
        try {
            List<Phone> phones = phoneService.filter(phone, userId, sort);
            List<PhoneDtoOut> phoneDtoOut = new ArrayList<>();
            for (Phone p : phones) {
                phoneDtoOut.add(phoneMapper.phoneDtoOut(p, new PhoneDtoOut()));
            }
            return phoneDtoOut;
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping
    public Phone createPhone(@RequestHeader HttpHeaders headers, @Valid @RequestBody PhoneDto phoneDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Phone phone = phoneMapper.dtoToObject(new Phone(), phoneDto);
            phoneService.createPhone(phone, user);
            return phone;
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (CustomException e) {
            throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED, e.getMessage());
        }
    }

    @PutMapping("/{phoneId}")
    public Phone updatePhone(@RequestHeader HttpHeaders headers, @PathVariable int phoneId, @Valid @RequestBody PhoneDto phoneDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Phone phone = phoneMapper.dtoToObject(phoneId, phoneDto);
            phoneService.updatePhone(phone, user);
            return phone;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{phoneId}")
    public void deletePhone(@RequestHeader HttpHeaders headers, @PathVariable int phoneId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            phoneService.deletePhone(phoneId, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
