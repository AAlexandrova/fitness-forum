package com.example.fitnessforum.controllers.rest;

import com.example.fitnessforum.exceptions.AuthorizationException;
import com.example.fitnessforum.exceptions.CustomException;
import com.example.fitnessforum.exceptions.EntityDuplicateException;
import com.example.fitnessforum.exceptions.EntityNotFoundException;
import com.example.fitnessforum.helpers.*;
import com.example.fitnessforum.models.*;
import com.example.fitnessforum.models.dto.*;
import com.example.fitnessforum.services.contracts.CommentService;
import com.example.fitnessforum.services.contracts.LikeService;
import com.example.fitnessforum.services.contracts.PostService;
import com.example.fitnessforum.services.contracts.TagService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Validated
@RestController
@RequestMapping("/api/posts")
public class PostRestController {

    private final PostService postService;
    private final PostMapper postMapper;
    private final AuthenticationHelper authenticationHelper;
    private final LikeMapper likeMapper;
    private final LikeService likeService;
    private final TagService tagService;
    private final CommentService commentService;
    private final CommentMapper commentMapper;
    private final TagMapper tagMapper;

    @Autowired
    public PostRestController(PostService postService, PostMapper postMapper,
                              AuthenticationHelper authenticationHelper,
                              LikeMapper likeMapper, LikeService likeService,
                              TagService tagService,
                              CommentService commentService, CommentMapper commentMapper, TagMapper tagMapper) {
        this.postService = postService;
        this.postMapper = postMapper;
        this.authenticationHelper = authenticationHelper;
        this.likeMapper = likeMapper;
        this.likeService = likeService;
        this.tagService = tagService;
        this.commentService = commentService;
        this.commentMapper = commentMapper;
        this.tagMapper = tagMapper;
    }

    @ApiResponse(responseCode = "200", description = "Success")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @Operation(summary = "Get all posts")
    @GetMapping
    public List<PostDtoOut> getPosts(
            @RequestHeader HttpHeaders headers,
            @RequestParam(required = false) String title,
            @RequestParam(required = false) String content,
            @RequestParam(required = false) Integer userId,
            @RequestParam(required = false) String username,
            @RequestParam(required = false) String tag,
            @RequestParam(required = false) String sortBy,
            @RequestParam(required = false) String sortOrder
    ) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            PostFilterOptions filterOptions = new PostFilterOptions(title, content, userId, username, tag, sortBy, sortOrder);
            List<Post> posts =  postService.getPosts(filterOptions);
            List<PostDtoOut> result = getPostsDtoOutListFromPost(posts);
            return result;
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    private List<PostDtoOut> getPostsDtoOutListFromPost(List<Post> posts) {
        List<PostDtoOut> result = new ArrayList<>();
        if (!posts.isEmpty()) {
            for (Post post : posts) {
                result.add(postMapper.postToDtoOut(post));
            }
        }
        return result;
    }

    @GetMapping("/most-liked")
    public List<PostDtoOut> getMostLikedPosts() {
        List<Post> posts = postService.getMostLikedPosts();
        List<PostDtoOut> result = getPostsDtoOutListFromPost(posts);
        return result;
    }

    @GetMapping("/most-commented")
    public List<PostDtoOut> getTenMostCommentedPosts() {
        List<Post> posts = postService.getTenMostCommentedPosts();
        List<PostDtoOut> result = getPostsDtoOutListFromPost(posts);
        return result;
    }

    @GetMapping("/most-recent")
    public List<PostDtoOut> getTenMostRecentPosts() {
        List<Post> posts = postService.getTenMostRecentPosts();
        List<PostDtoOut> result = getPostsDtoOutListFromPost(posts);
        return result;
    }

    @GetMapping("/{postId}")
    public PostDtoOut getPostById(@RequestHeader HttpHeaders headers, @PathVariable int postId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = postService.getPostById(postId);
            return postMapper.postToDtoOut(post);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Post createPost(@RequestHeader HttpHeaders headers, @Valid @RequestBody PostDto postDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = postMapper.dtoToPost(postDto);
            postService.createPost(post, user);
            return post;
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{postId}")
    public PostDtoOut updatePost(@RequestHeader HttpHeaders headers, @PathVariable int postId, @Valid @RequestBody PostDto postDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = postMapper.dtoToPost(postId, postDto);
            postService.updatePost(post, user);
            return postMapper.postToDtoOut(post);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{postId}")
    public void deletePost(@RequestHeader HttpHeaders headers, @PathVariable int postId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            postService.deletePost(postId, user);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    //добавяне на лайк към пост
    @PutMapping("/{postId}/likes")
    public LikeDtoOut createLike(@RequestHeader HttpHeaders headers
            , @PathVariable int postId
            , @Valid @RequestBody LikeDto likeDto) {

        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = postService.getPostById(postId);
            Like like = likeMapper.dtoToLike(likeDto, user, post);
            likeService.createLike(like);
            return likeMapper.likeToDtoOut(like);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
        //Ако няма такъв пост:
        catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        //Ако този юзър вече харесва този пост или се опитва да хареса свой собствен пост:
        catch (CustomException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    //изтриване на лайк към пост
    @DeleteMapping("/{postId}/likes")
    public void deleteLike(@RequestHeader HttpHeaders headers, @PathVariable int postId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = postService.getPostById(postId);
            Like like = likeService.getLikeByPostAndUser(post, user);
            likeService.deleteLike(like.getLikeId());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
        //Ако този пост не съществува или няма лайк на този юзър към този пост:
        catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    //показване на всички лайкове към пост
    @GetMapping("/{postId}/likes")
    public List<LikeDtoOut> getLikes(@RequestHeader HttpHeaders headers,
                                     @PathVariable int postId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = postService.getPostById(postId);
            List<Like> likes = likeService.getLikesByPostId(postId);
            List<LikeDtoOut> result = new ArrayList<>();
            if (!likes.isEmpty()) {
                for (Like like : likes) {
                    result.add(likeMapper.likeToDtoOut(like));
                }
            }
            return result;
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    //добавяне на таг към пост:
    @PutMapping("/{postId}/tags")
    public PostDtoOut addTag(@RequestHeader HttpHeaders headers, @PathVariable int postId,
                             @Valid @RequestBody TagDto tagDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = postService.getPostById(postId);
            Tag tag = tagMapper.dtoToTag(tagDto);
            Tag repositoryTag = tagService.getOrCreateTag(user, tag);

            tagService.addTagToPost(user, post, repositoryTag);
            return postMapper.postToDtoOut(post);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
        //Ако няма такъв пост:
        catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        //Ако този пост вече има такъв таг, няма да се добави, защото таговете са в сет
    }



    //редактиране на конкретен таг на конкретен пост:
    @PutMapping("/{postId}/tags/{tagId}")
    public Post updateTagOnPost(@RequestHeader HttpHeaders headers, @PathVariable int postId,
                                @PathVariable int tagId, @Valid @RequestBody TagDto tagDto) {

        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = postService.getPostById(postId);
            Tag repositoryTag = tagService.getTagById(tagId);
            Tag newTag = tagMapper.dtoToTag(tagDto);
            tagService.updateTagOnPost(user, post, tagId, newTag);
            return post;
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
        //Ако няма такъв пост или таг:
        catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        //Ако този пост вече има новия таг или ако този пост няма стария таг:
        catch (CustomException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    //изтриване на конкретен таг от конкретен пост
    @DeleteMapping("/{postId}/tags/{tagId}")
    public void deleteTagFromPost(@RequestHeader HttpHeaders headers, @PathVariable int postId,
                                  @PathVariable int tagId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = postService.getPostById(postId);
            tagService.deleteTagFromPost(user, post, tagId);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
        //ако този пост или този таг го няма по принцип ИЛИ ако този пост няма такъв таг:
        catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{postId}/comments")
    public List<CommentDtoOut> getComments(@RequestHeader HttpHeaders headers, @PathVariable int postId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            List<Comment> comments = commentService.getCommentByPostId(postId);
            List<CommentDtoOut> commentsForPost = new ArrayList<>();
            for (Comment comment : comments) {
                commentsForPost.add(commentMapper.commentDtoOut(comment, new CommentDtoOut()));
            }
            return commentsForPost;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{postId}/comments/{commentId}")
    public CommentDtoOut getCommentById(@RequestHeader HttpHeaders headers, @PathVariable int postId, @PathVariable int commentId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Comment comment = commentService.getCommentById(commentId, postId);
            return commentMapper.commentDtoOut(comment, new CommentDtoOut());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{postId}/comments/filter")
    public List<CommentDtoOut> filterComments(
            @RequestHeader HttpHeaders headers,
            @PathVariable Integer postId,
            @RequestParam(name = "commentId", required = false) Optional<Integer> commentId,
            @RequestParam(name = "userid", required = false) Optional<Integer> userId,
            @RequestParam(name = "sort", required = false) Optional<String> sort) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            List<Comment> comments = commentService.filter(postId, commentId, userId, sort);
            List<CommentDtoOut> commentsForPost = new ArrayList<>();
            for (Comment comment : comments) {
                commentsForPost.add(commentMapper.commentDtoOut(comment, new CommentDtoOut()));
            }
            return commentsForPost;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping("/{postId}/comments")
    public Comment createComment(@PathVariable int postId, @RequestHeader HttpHeaders headers, @Valid @RequestBody CommentDto commentDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Comment comment = commentMapper.dtoToObject(postId, user.getUserId(), commentDto);
            commentService.createComment(comment, user);
            return comment;
        }
        catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{postId}/comments/{commentId}")
    public Comment updateComment(@RequestHeader HttpHeaders headers, @PathVariable int postId,
                                 @PathVariable int commentId, @Valid @RequestBody CommentDto commentDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = postService.getPostById(postId);
            Comment commentById = commentService.getCommentById(commentId, postId);
            Comment comment = commentMapper.dtoToObject(commentById, commentDto);
            commentService.updateComment(comment, user, post);
            return commentService.getCommentById(commentId, postId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{postId}/comments/{commentId}")
    public void deleteComment(@RequestHeader HttpHeaders headers, @PathVariable int commentId, @PathVariable int postId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = postService.getPostById(postId);
            commentService.deleteComment(commentId, user, post);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
