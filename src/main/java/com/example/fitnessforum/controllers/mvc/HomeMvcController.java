package com.example.fitnessforum.controllers.mvc;

import com.example.fitnessforum.helpers.AuthenticationHelper;
import com.example.fitnessforum.helpers.PostMapper;
import com.example.fitnessforum.models.Post;
import com.example.fitnessforum.models.dto.PostDtoOut;
import com.example.fitnessforum.services.contracts.PostService;
import com.example.fitnessforum.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final UserService userService;
    private final PostService postService;
    private final PostMapper postMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public HomeMvcController(UserService userService, PostService postService,
                             PostMapper postMapper, AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.postService = postService;
        this.postMapper = postMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currentUserId")
    public Integer populateCurrentUserId(HttpSession session) {
        return (Integer) session.getAttribute("currentUserId");
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        return session.getAttribute("isAdmin") != null;
    }


    @GetMapping
    public String showHomePage(Model model) {
        List<Post> posts = postService.getTenMostCommentedPosts();
        model.addAttribute("postsCount", postService.getPostsCount());
        model.addAttribute("usersCount", userService.getUsersCount());
        model.addAttribute("tenMostCommentedPosts", getPostsDtoOutListFromPost(posts));
        return "index";
    }

    @GetMapping("/recentPosts")
    public String showHomePage2(Model model) {
        List<Post> posts = postService.getTenMostRecentPosts();
        model.addAttribute("postsCount", postService.getPostsCount());
        model.addAttribute("usersCount", userService.getUsersCount());
        model.addAttribute("tenMostRecentPosts", getPostsDtoOutListFromPost(posts));
        return "index2";
    }

    private List<PostDtoOut> getPostsDtoOutListFromPost(List<Post> posts) {
        List<PostDtoOut> result = new ArrayList<>();
        if (!posts.isEmpty()) {
            for (Post post : posts) {
                result.add(postMapper.postToDtoOut(post));
            }
        }
        return result;
    }
}
