package com.example.fitnessforum.controllers.mvc;

import com.example.fitnessforum.exceptions.AuthorizationException;
import com.example.fitnessforum.exceptions.EntityDuplicateException;
import com.example.fitnessforum.exceptions.EntityNotFoundException;
import com.example.fitnessforum.helpers.AuthenticationHelper;
import com.example.fitnessforum.helpers.CommentMapper;
import com.example.fitnessforum.helpers.PostMapper;
import com.example.fitnessforum.helpers.UserMapper;
import com.example.fitnessforum.models.Comment;
import com.example.fitnessforum.models.CommentFilterOptions;
import com.example.fitnessforum.models.User;
import com.example.fitnessforum.models.dto.CommentDto;
import com.example.fitnessforum.models.dto.CommentFilterDto;
import com.example.fitnessforum.services.contracts.CommentService;
import com.example.fitnessforum.services.contracts.PostService;
import com.example.fitnessforum.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/posts")
public class CommentMvcController {
    public final CommentService commentService;
    public final CommentMapper commentMapper;
    private final UserService userService;
    private final UserMapper userMapper;
    private final PostService postService;
    private final PostMapper postMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CommentMvcController(CommentService commentService,
                                CommentMapper commentMapper,
                                UserService userService,
                                UserMapper userMapper,
                                PostService postService,
                                PostMapper postMapper,
                                AuthenticationHelper authenticationHelper) {
        this.commentService = commentService;
        this.commentMapper = commentMapper;
        this.userService = userService;
        this.userMapper = userMapper;
        this.postService = postService;
        this.postMapper = postMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currentUserId")
    public Integer populateCurrentUserId(HttpSession session) {
        return (Integer) session.getAttribute("currentUserId");
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        return session.getAttribute("isAdmin") != null;
    }

    @GetMapping("/{postId}/comments")
    public String showAllCommentsForOnePosts(
            @ModelAttribute("filter") CommentFilterDto commentFilterDto,
            @PathVariable int postId,
            HttpSession session,
            Model model) {
        try {
            CommentFilterOptions commentFilterOptions = new CommentFilterOptions(
                    commentFilterDto.getUsername(),
                    commentFilterDto.getContent(),
                    commentFilterDto.getSortBy(),
                    commentFilterDto.getOrderBy());
            User user = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("user", user);
            model.addAttribute("commentList", commentService.getCommentByPostId(postId, commentFilterOptions));
            model.addAttribute("post", postService.getPostById(postId));
            model.addAttribute("filter", commentFilterDto);
            model.addAttribute("postId", postId);
            return "CommentsView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
    }

    @GetMapping("/{postId}/comments/{commentId}")
    public String showOneCommentForOnePost(HttpSession session,
                                           @PathVariable int commentId,
                                           @PathVariable int postId, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            model.addAttribute(commentService.getCommentById(commentId, postId));
            model.addAttribute("postId", postId);
            model.addAttribute("post", postService.getPostById(postId));
            model.addAttribute("user", user);
            return "CommentView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
    }

    @GetMapping("/{postId}/comments/new")
    public String showCreateCommentPostPage(@PathVariable int postId,
                                            HttpSession session,
                                            Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        model.addAttribute("postId", postId);
            model.addAttribute("comment", new CommentDto());
            model.addAttribute("post", postService.getPostById(postId));
            return "CommentsCreateView";
    }

    @PostMapping("/{postId}/comments/new")
    public String createCommentPost(@Valid @ModelAttribute("comment") CommentDto commentDto,
                                    BindingResult bindingResult,
                                    @PathVariable int postId,
                                    HttpSession session,
                                    Model model) {
        User user;
        //Ако потребителят не се е логнал
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        if (bindingResult.hasErrors()) {
            model.addAttribute("post", postService.getPostById(postId));
            return "CommentsCreateView";
        }
        try {
            Comment comment = commentMapper.dtoToObject(postId, user.getUserId(), commentDto);
            commentService.createComment(comment, user);
            return "redirect:/posts/{postId}/comments";
        }
        //Ако е блокиран
        catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
        catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{postId}/comments/{commentId}/update")
    public String showEditCommentPage(@PathVariable int postId,
                                      @PathVariable int commentId,
                                      HttpSession session,
                                      Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            CommentDto commentDto = commentMapper.commentDtoMvc(commentService.getCommentById(commentId, user));
            model.addAttribute("commentId", commentId);
            model.addAttribute("postId", postId);
            model.addAttribute("comment", commentDto);
            return "CommentUpdateView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{postId}/comments/{commentId}/update")
    public String updateComment(@PathVariable int postId,
                                @PathVariable int commentId,
                                @Valid @ModelAttribute("comment") CommentDto commentDto,
                                BindingResult bindingResult,
                                HttpSession session,
                                Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        if (bindingResult.hasErrors()) {
            return "CommentUpdateView";
        }
        try {
            Comment comment = commentService.getCommentById(commentId, user);
            Comment updatedComment = commentMapper.dtoToObject(comment, commentDto);
            commentService.updateComment(updatedComment, user);
            return "redirect:/posts/{postId}/comments";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
        catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{postId}/comments/{commentId}/delete")
    public String showDeleteCommentPage(@PathVariable int postId,
                                        @PathVariable int commentId,
                                        HttpSession session,
                                        Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Comment comment = commentService.getCommentById(commentId, user);
            model.addAttribute("commentId", commentId);
            model.addAttribute("postId", postId);
            model.addAttribute("comment", comment);
            return "CommentDeleteView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{postId}/comments/{commentId}/delete")
    public String deleteComment(@PathVariable int postId,
                                @PathVariable int commentId,
                                HttpSession session,
                                Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            commentService.deleteComment(commentId, user);
            return "redirect:/posts/{postId}/comments";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
        catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }
}
