package com.example.fitnessforum.controllers.mvc;

import com.example.fitnessforum.exceptions.AuthorizationException;
import com.example.fitnessforum.exceptions.CustomException;
import com.example.fitnessforum.exceptions.EntityDuplicateException;
import com.example.fitnessforum.exceptions.EntityNotFoundException;
import com.example.fitnessforum.helpers.*;
import com.example.fitnessforum.models.*;
import com.example.fitnessforum.models.dto.*;
import com.example.fitnessforum.services.contracts.LikeService;
import com.example.fitnessforum.services.contracts.PostService;
import com.example.fitnessforum.services.contracts.TagService;
import com.example.fitnessforum.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.*;

import static java.lang.Character.isLowerCase;

@Controller
@RequestMapping("/posts")
public class PostMvcController {
    private final PostService postService;
    private final PostMapper postMapper;
    private final UserService userService;
    private final LikeService likeService;
    private final LikeMapper likeMapper;
    private final TagService tagService;
    private final TagMapper tagMapper;
    private final AuthenticationHelper authenticationHelper;
    private final PostFilterOptionsMapper postFilterOptionsMapper;

    @Autowired
    public PostMvcController(PostService postService, PostMapper postMapper,
                             UserService userService, LikeService likeService,
                             LikeMapper likeMapper, TagService tagService, TagMapper tagMapper, AuthenticationHelper authenticationHelper, PostFilterOptionsMapper postFilterOptionsMapper) {
        this.postService = postService;
        this.postMapper = postMapper;
        this.userService = userService;
        this.likeService = likeService;
        this.likeMapper = likeMapper;
        this.tagService = tagService;
        this.tagMapper = tagMapper;
        this.authenticationHelper = authenticationHelper;
        this.postFilterOptionsMapper = postFilterOptionsMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currentUserId")
    public Integer populateCurrentUserId(HttpSession session) {
        return (Integer) session.getAttribute("currentUserId");
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        return session.getAttribute("isAdmin") != null;
    }

    @ModelAttribute("usersWithPosts")
    public List<User> populateUsersWithPosts() {
        return userService.getUsersWithPosts();
    }

    @GetMapping
    public String getPosts(@ModelAttribute("postFilterDto") PostFilterDto filterDto,
                           HttpSession session, Model model) {
        try {
            PostFilterOptions filterOptions = postFilterOptionsMapper.dtoToObject(filterDto);
            User user = authenticationHelper.tryGetCurrentUser(session);
            List<Post> posts = postService.getPosts(filterOptions);
            List<PostDtoOut> result = getPostsDtoOutListFromPost(posts);
            //Този мап е, за да се показват бутони за добавяне на лайк, ако този юзър не е харесал поста
            // или за махане на лайк, ако вече го е харесал.
            Map<Integer, Boolean> map = new HashMap<>();
            for (PostDtoOut post : result) {
                map.put(post.getPostId(), likeService.userLikesPost(post.getPostId(), user.getUserId()));
            }
            model.addAttribute("map", map);
            model.addAttribute("user", user);
            model.addAttribute("postDtoOuts", result);
            model.addAttribute("postFilterDto", filterDto);
            return "PostsViewFilter";
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
    }

    @GetMapping("/most-liked")
    public String showMostLikedPosts(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetCurrentUser(session);
            List<Post> posts = postService.getMostLikedPosts();
            List<PostDtoOut> result = getPostsDtoOutListFromPost(posts);
            model.addAttribute("mostLikedPosts", result);
            model.addAttribute("user", user);
            return "MostLikedPostsView";
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
    }

    @GetMapping("/{postId}")
    public String showSinglePost(@PathVariable int postId, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetCurrentUser(session);
            Post post = postService.getPostById(postId);
            PostDtoOut postDtoOut = postMapper.postToDtoOut(post);
            model.addAttribute("postDtoOut", postDtoOut);
            model.addAttribute("post", post);
            model.addAttribute("user", user);
            return "PostView";
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }


    @GetMapping("/new")
    public String showNewPostPage(HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        model.addAttribute("postDto", new PostDto());
        return "PostCreateView";
    }

    @PostMapping("/new")
    public String createPost(@Valid @ModelAttribute("postDto") PostDto postDto,
                             BindingResult bindingResult, HttpSession session, Model model) {
        User user;
        //Ако потребителят не се е логнал
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        if (bindingResult.hasErrors()) {
            return "PostCreateView";
        }

        try {
            Post post = postMapper.dtoToPost(postDto);
            postService.createPost(post, user);
            return "redirect:/posts/" + post.getPostId();
        }
        //Ако вече логнатият потребител е блокиран:
        catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityDuplicateException e) {
            bindingResult.rejectValue("title", "duplicate_title", e.getMessage());
            return "PostCreateView";
        }
    }

    @GetMapping("/{postId}/update")
    public String showUpdatePostPage(@PathVariable int postId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Post post = postService.getPostById(postId);
            PostDto postDto = postMapper.postToDto(post);
            model.addAttribute("postId", postId);
            model.addAttribute("postDto", postDto);
            return "PostUpdateView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("{postId}/update")
    public String updatePost(@PathVariable int postId,
                             @Valid @ModelAttribute("postDto") PostDto postDto,
                             BindingResult bindingResult,
                             HttpSession session,
                             Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        if (bindingResult.hasErrors()) {
            return "PostUpdateView";
        }
        try {
            Post existingPost = postService.getPostById(postId);
            Post updatedPost = postMapper.dtoToPostMvc(existingPost, postDto);
            postService.updatePost(updatedPost, user);
            return "redirect:/posts/" + postId;
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
        //Ако този пост не съществува
        catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
        //Ако се опитваме да създадем два поста с еднакъв title
        catch (EntityDuplicateException e) {
            bindingResult.rejectValue("title", "duplicate_title", e.getMessage());
            return "PostUpdateView";
        }
    }

    @GetMapping("/{postId}/delete")
    public String showDeletePostPage(@PathVariable int postId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            model.addAttribute("postId", postId);
            return "PostDeleteView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{postId}/delete")
    public String deletePost(@PathVariable int postId, HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            postService.deletePost(postId, user);
            return "redirect:/posts";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    //createLike
    @GetMapping("/{postId}/likes/new")
    public String showNewLikePage(@PathVariable int postId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            postService.getPostById(postId);
            model.addAttribute("likeDto", new LikeDto());
            return "LikeCreateView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{postId}/likes/new")
    public String createLike(@PathVariable int postId,
                             @Valid @ModelAttribute("likeDto") LikeDto likeDto,
                             BindingResult bindingResult,
                             HttpSession session,
                             Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        if (bindingResult.hasErrors()) {
            return "LikeCreateView";
        }
        try {
            Post post = postService.getPostById(postId);
            Like like = likeMapper.dtoToLike(likeDto, user, post);
            likeService.createLike(like);
            return "redirect:/posts/" + postId;
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
        //Ако няма такъв пост:
        catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
        //Ако този юзър вече харесва този пост или се опитва да хареса свой собствен пост:
        catch (CustomException e) {
            model.addAttribute("error", e.getMessage());
            return "CustomExceptionView";
        }
    }

    //изтриване на лайк към пост
    @GetMapping("/{postId}/likes/delete")
    public String showDeleteLikePage(@PathVariable int postId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            model.addAttribute("postId", postId);
            return "LikeDeleteView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{postId}/likes/delete")
    public String deleteLikeFromPost(@PathVariable int postId, HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Post post = postService.getPostById(postId);
            Like like = likeService.getLikeByPostAndUser(post, user);
            likeService.deleteLike(like.getLikeId());
            return "redirect:/posts/{postId}";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
        //Ако този пост не съществува или няма лайк на този юзър към този пост:
        catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    //показване на всички лайкове към пост
    @GetMapping("/{postId}/likes")
    public String showPostLikes(@PathVariable int postId, HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("user", user);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Post post = postService.getPostById(postId);
            model.addAttribute("post", post);
            boolean postHasLikes = true;
            try {
                List<Like> likes = likeService.getLikesByPostId(postId);
            } catch (EntityNotFoundException e) {
                postHasLikes = false;
            }
            List<LikeDtoOut> result = new ArrayList<>();
            if (postHasLikes) {
                List<Like> likes = likeService.getLikesByPostId(postId);
                if (!likes.isEmpty()) {
                    for (Like like : likes) {
                        result.add(likeMapper.likeToDtoOut(like));
                    }
                }
            }
            model.addAttribute("likesToSinglePost", result);

            boolean currentUserLikesThisPost = true;
            try {
                likeService.getLikeByPostAndUser(post, user);
            } catch (EntityNotFoundException e) {
                currentUserLikesThisPost = false;
            }
            model.addAttribute("currentUserLikesThisPost", currentUserLikesThisPost);
            return "LikesToSinglePostView";
        }
        //Ако този пост го няма - това нужно ли е тук, при положение че ще тръгваме от страницата с този пост
        catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    //добавяне на много тагове наведнъж
//добавяне на списък от тагове към пост
    @GetMapping("/{postId}/tags/list")
    public String showAddTagsPage(@PathVariable int postId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            postService.getPostById(postId);
            TagsCreateDto tagsForm = new TagsCreateDto();
            for (int i = 0; i < 10; i++) {
                tagsForm.addTag(new TagDto());
            }
            model.addAttribute("tagsForm", tagsForm);
            return "TagsAddToPostView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{postId}/tags/list")
    public String addNewTagsToPost(@PathVariable int postId,
                                   @Valid @ModelAttribute("tagsForm") TagsCreateDto tagForm,
                                   BindingResult bindingResult,
                                   HttpSession session,
                                   Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }

        try {
            Post post = postService.getPostById(postId);
            for (TagDto tagDto : tagForm.getInputTags()) {
                if (!isTagValid(tagDto.getTitle())) {
                    FieldError error = new FieldError("tagDto", "title",
                            "Tags should be lowercase.");
                    bindingResult.addError(error);
//                    bindingResult.reject("duplicate_title", "Tags should be lowercase.");
                    return "TagsAddToPostView";
                }
                if (!tagDto.getTitle().isEmpty()) {
                    Tag tag = tagMapper.dtoToTag(tagDto);
                    Tag repositoryTag = tagService.getOrCreateTag(user, tag);
                    tagService.addTagToPost(user, post, repositoryTag);
                }
            }
            model.addAttribute("postId", postId);
            return "redirect:/posts/" + postId;
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
        //Ако няма такъв пост:
        catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
        //Ако този пост вече има такъв таг:
        catch (CustomException e) {
            model.addAttribute("error", e.getMessage());
            return "CustomExceptionView";
        }
    }

    //добавяне на нов таг към пост
    @GetMapping("/{postId}/tags/new")
    public String showNewTagPage(@PathVariable int postId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            postService.getPostById(postId);
            model.addAttribute("tagDto", new TagDto());
            return "TagCreateView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{postId}/tags/new")
    public String addNewTagToPost(@PathVariable int postId,
                                  @Valid @ModelAttribute("tagDto") TagDto tagDto,
                                  BindingResult bindingResult,
                                  HttpSession session,
                                  Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        if (bindingResult.hasErrors()) {
            return "TagCreateView";
        }
        try {
            Post post = postService.getPostById(postId);
            Tag tag = tagMapper.dtoToTag(tagDto);
            Tag repositoryTag = tagService.getOrCreateTag(user, tag);
            tagService.addTagToPost(user, post, repositoryTag);
            return "redirect:/posts/" + postId;
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
        //Ако няма такъв пост:
        catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    //редактиране на конкретен таг на конкретен пост
    @GetMapping("/{postId}/tags/{tagId}/update")
    public String showEditTagPage(@PathVariable int postId, @PathVariable int tagId,
                                  HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            postService.getPostById(postId);
            model.addAttribute("postId", postId);
            Tag tag = tagService.getTagById(tagId);
            TagDto tagDto = tagMapper.tagToDto(tag);
            model.addAttribute("tagDto", tagDto);
            return "TagUpdateView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{postId}/tags/{tagId}/update")
    public String updateTagOnPost(@PathVariable int postId,
                                  @PathVariable int tagId,
                                  @Valid @ModelAttribute("tagDto") TagDto tagDto,
                                  BindingResult bindingResult,
                                  HttpSession session,
                                  Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        if (bindingResult.hasErrors()) {
            return "TagCreateView";
        }
        try {
            Post post = postService.getPostById(postId);
            Tag newTag = tagMapper.dtoToTag(tagId, tagDto);
            tagService.updateTagOnPost(user, post, tagId, newTag);
            return "redirect:/posts/" + postId;
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } //Ако този пост вече има новия таг или ако този пост няма стария таг:
        catch (CustomException e) {
            model.addAttribute("error", e.getMessage());
            return "CustomExceptionView";
        }
    }

    //показване на всички тагове към пост
    @GetMapping("/{postId}/tags")
    public String showPostTags(@PathVariable int postId, HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Post post = postService.getPostById(postId);
            Set<Tag> tags = post.getTags();
            List<Tag> result = new ArrayList<>();
            if (!tags.isEmpty()) {
                result.addAll(tags);
            }
            model.addAttribute("user", user);
            model.addAttribute("post", post);
            model.addAttribute("tagsToSinglePost", result);
            return "TagsToSinglePostView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    //изтриване на конкретен таг от конкретен пост
    @GetMapping("/{postId}/tags/{tagId}/delete")
    public String showDeleteTagPage(@PathVariable int postId, @PathVariable int tagId,
                                    HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Post post = postService.getPostById(postId);
            Tag tag = tagService.getTagById(tagId);
            model.addAttribute("tagId", tagId);
            model.addAttribute("postId", postId);
            return "TagDeleteView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{postId}/tags/{tagId}/delete")
    public String deleteTagFromPost(@PathVariable int postId,
                                    @PathVariable int tagId,
                                    HttpSession session,
                                    Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Post post = postService.getPostById(postId);
            tagService.deleteTagFromPost(user, post, tagId);
            return "redirect:/posts/{postId}";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    private List<PostDtoOut> getPostsDtoOutListFromPost(List<Post> posts) {
        List<PostDtoOut> result = new ArrayList<>();
        if (!posts.isEmpty()) {
            for (Post post : posts) {
                result.add(postMapper.postToDtoOut(post));
            }
        }
        return result;
    }

    private boolean isTagValid(String tagDto) {
        if (tagDto == null || tagDto.isEmpty()) {
            return true;
        }
        char[] chars = tagDto.toCharArray();
        Character[] characters = new Character[chars.length];

        for (int i = 0; i < chars.length; i++) {
            characters[i] = chars[i];
        }
        boolean isLowerCase = true;
        for (Character c : characters) {
            if (!(isLowerCase(c) || c.equals(' '))) {
                isLowerCase = false;
            }
        }
        return isLowerCase;
    }
}

