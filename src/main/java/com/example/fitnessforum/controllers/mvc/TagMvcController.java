package com.example.fitnessforum.controllers.mvc;

import com.example.fitnessforum.exceptions.AuthorizationException;
import com.example.fitnessforum.exceptions.EntityDuplicateException;
import com.example.fitnessforum.exceptions.EntityNotFoundException;
import com.example.fitnessforum.helpers.AuthenticationHelper;
import com.example.fitnessforum.helpers.TagMapper;
import com.example.fitnessforum.models.Tag;
import com.example.fitnessforum.models.User;
import com.example.fitnessforum.models.dto.TagDto;
import com.example.fitnessforum.services.contracts.TagService;
import com.example.fitnessforum.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/tags")
public class TagMvcController {
    private final TagService tagService;
    private final UserService userService;
    private final TagMapper tagMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public TagMvcController(TagService tagService, UserService userService,
                            TagMapper tagMapper, AuthenticationHelper authenticationHelper) {
        this.tagService = tagService;
        this.userService = userService;
        this.tagMapper = tagMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currentUserId")
    public Integer populateCurrentUserId(HttpSession session) {
        return (Integer) session.getAttribute("currentUserId");
    }

    @GetMapping
    public String showAllTags(HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            List<Tag> tags = tagService.getTags(user);
            model.addAttribute("user", user);
            model.addAttribute("tags", tags);
            return "TagsView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    //getTagById - тук няма нужда да се показва само един конкретен таг
    // , защото той не съдържа никаква инфо освен id, title
    @GetMapping("/{tagId}/update")
    public String showEditTagPage(@PathVariable int tagId, HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Tag tag = tagService.getTagById(user, tagId);
            TagDto tagDto = tagMapper.tagToDto(tag);
            model.addAttribute("tagId", tagId);
            model.addAttribute("tagDto", tagDto);
            return "TagUpdateAsAdmin";
        } catch (AuthorizationException e) {
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{tagId}/update")
    public String updateTag(@PathVariable int tagId,
                            @Valid @ModelAttribute("tagDto") TagDto tagDto,
                            BindingResult bindingResult,
                            HttpSession session,
                            Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        if (bindingResult.hasErrors()) {
            return "TagUpdateAsAdmin";
        }
        try {
            Tag tag = tagMapper.dtoToTag(tagId, tagDto);
            tagService.updateTag(user, tag);
            return "redirect:/tags";
        } catch (AuthorizationException e) {
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } //Ако вече съществува такъв таг:
        catch (EntityDuplicateException e) {
            model.addAttribute("error", e.getMessage());
            return "CustomExceptionView";
        }
    }

    @GetMapping("/{tagId}/delete")
    public String showDeleteTagPage(@PathVariable int tagId, HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Tag tag = tagService.getTagById(user, tagId);
            model.addAttribute("tagId", tagId);
            return "TagDeleteAsAdmin";
        } catch (AuthorizationException e) {
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{tagId}/delete")
    public String deleteTag(@PathVariable int tagId,
                            HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            tagService.deleteTag(user, tagId);
            return "redirect:/tags";
        } catch (AuthorizationException e) {
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }
}
