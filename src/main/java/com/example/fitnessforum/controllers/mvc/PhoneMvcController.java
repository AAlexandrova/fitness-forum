package com.example.fitnessforum.controllers.mvc;

import com.example.fitnessforum.exceptions.AuthorizationException;
import com.example.fitnessforum.exceptions.CustomException;
import com.example.fitnessforum.exceptions.EntityDuplicateException;
import com.example.fitnessforum.exceptions.EntityNotFoundException;
import com.example.fitnessforum.helpers.AuthenticationHelper;
import com.example.fitnessforum.helpers.PhoneMapper;
import com.example.fitnessforum.helpers.UserMapper;
import com.example.fitnessforum.models.Phone;
import com.example.fitnessforum.models.User;
import com.example.fitnessforum.models.dto.PhoneDtoMvc;
import com.example.fitnessforum.services.contracts.PhoneService;
import com.example.fitnessforum.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/phones")
public class PhoneMvcController {

    public final PhoneService phoneService;
    public final PhoneMapper phoneMapper;
    private final UserService userService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public PhoneMvcController(PhoneService phoneService, PhoneMapper phoneMapper,
                              UserService userService, UserMapper userMapper,
                              AuthenticationHelper authenticationHelper) {
        this.phoneService = phoneService;
        this.phoneMapper = phoneMapper;
        this.userService = userService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currentUserId")
    public Integer populateCurrentUserId(HttpSession session) {
        return (Integer) session.getAttribute("currentUserId");
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        return session.getAttribute("isAdmin") != null;
    }

    @GetMapping
    public String showAllAdminPhones(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute(phoneService.getPhones());
            model.addAttribute("user", user);
            model.addAttribute("isAdminUser", user.isAdmin());
            return "PhonesView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException e) {
            model.addAttribute(phoneService.getPhones());
            return "PhonesView";
        }
    }

    @GetMapping("/{phoneId}")
    public String showSingleAdminPhone(HttpSession session, @PathVariable int phoneId, Model model) {
        try {
            User user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            model.addAttribute(phoneService.getPhoneById(phoneId));
            return "PhoneView";
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/new")
    public String showCreatePhonePage(HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
            model.addAttribute("phone", new PhoneDtoMvc());
            return "PhoneCreateView";
    }

    @PostMapping("/new")
    public String createPhone(@Valid @ModelAttribute("phone") PhoneDtoMvc phoneDtoMvc,
                              BindingResult bindingResult, HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        if (bindingResult.hasErrors()) {
            return "PhoneCreateView";
        }
        try {
            Phone phone = phoneMapper.dtoToObjectForCreate(phoneDtoMvc);
            phoneService.createPhone(phone, user);
            return "redirect:/phones";
        }  catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
        catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (EntityDuplicateException e) {
            bindingResult.rejectValue("userId", "duplicate_userId", e.getMessage());
            return "PhoneCreateView";
        } catch (CustomException e) {
            model.addAttribute("error", e.getMessage());
            return "CustomExceptionView";
        }
    }


    @GetMapping("/{phoneId}/update")
    public String showEditPhonePage(@PathVariable int phoneId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            PhoneDtoMvc phoneDtoMvc = phoneMapper.toDto(phoneService.getPhoneById(phoneId));
            model.addAttribute("phoneId", phoneId);
            model.addAttribute("phone", phoneDtoMvc);
            return "PhoneUpdateView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{phoneId}/update")
    public String updatePhone(@PathVariable int phoneId, @Valid @ModelAttribute("phone") PhoneDtoMvc phoneDtoMvc,
                              HttpSession session, BindingResult bindingResult, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        if (bindingResult.hasErrors()) {
            return "PhoneUpdateView";
        }
        try {
            Phone updatedPhone = phoneService.getPhoneById(phoneId);
            Phone phone = phoneMapper.dtoToObjectMvc(updatedPhone, phoneDtoMvc);
            phoneService.updatePhone(phone, user);
            return "redirect:/phones/{phoneId}";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
        catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (EntityDuplicateException e) {
            bindingResult.rejectValue("phone", "duplicate_phone", e.getMessage());
            return "PhoneUpdateView";
        } catch (CustomException e) {
            model.addAttribute("error", e.getMessage());
            return "CustomExceptionView";
        }
    }

    @GetMapping("/{phoneId}/delete")
    public String showDeletePhonePage(@PathVariable int phoneId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Phone phone = phoneService.getPhoneById(phoneId);
            model.addAttribute("phoneId", phoneId);
            model.addAttribute("phone", phone);
            return "PhoneDeleteView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{phoneId}/delete")
    public String deletePhone(@PathVariable int phoneId, HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            phoneService.deletePhone(phoneId, user);
            return "redirect:/phones";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
        catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }
}
