package com.example.fitnessforum.controllers.mvc;

import com.example.fitnessforum.exceptions.AuthorizationException;
import com.example.fitnessforum.exceptions.EntityDuplicateException;
import com.example.fitnessforum.exceptions.EntityNotFoundException;
import com.example.fitnessforum.helpers.AuthenticationHelper;
import com.example.fitnessforum.helpers.UserMapper;
import com.example.fitnessforum.models.User;
import com.example.fitnessforum.models.UserFilterOptions;
import com.example.fitnessforum.models.dto.UserDto;
import com.example.fitnessforum.models.dto.UserFilterDto;
import com.example.fitnessforum.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.BindingResult;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserMvcController(UserService userService,
                             UserMapper userMapper,
                             AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currentUserId")
    public Integer populateCurrentUserId(HttpSession session) {
        return (Integer) session.getAttribute("currentUserId");
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        return session.getAttribute("isAdmin") != null;
    }

    @GetMapping
    public String showAllUsers(@ModelAttribute("filter") UserFilterDto userFilterDto,
                               HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            UserFilterOptions userFilterOptions = new UserFilterOptions(
                    userFilterDto.getFirstName(),
                    userFilterDto.getLastName(),
                    userFilterDto.getEmail(),
                    userFilterDto.getAdmin(),
                    userFilterDto.getBlocked(),
                    userFilterDto.getUsername(),
                    userFilterDto.getSortBy(),
                    userFilterDto.getOrderBy());
            User admin = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute(userService.getUsers(userFilterOptions));
            model.addAttribute("user", admin);
            model.addAttribute("filter", userFilterDto);
            model.addAttribute("isAdminUser", admin.isAdmin());
            return "UsersView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{userId}")
    public String showSingleUser(@PathVariable int userId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            User user = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute(userService.getUserById(userId));
            model.addAttribute("user", user);
            return "UserView";
        } catch (EntityNotFoundException e) {
            return "NotFoundView";
        }
    }


    @GetMapping("/new")
    public String showCreateUserPage(Model model) {
        model.addAttribute("user", new UserDto());
        return "UserCreateView";
    }

    @PostMapping("/new")
    public String createUser(@Valid @ModelAttribute("user") UserDto userDto,
                             BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            return "UserCreateView";
        }
        try {
            User user = userMapper.dtoToObject(userDto);
            userService.createUser(user);
            return "redirect:/authentication/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (EntityDuplicateException e) {
            bindingResult.rejectValue("username", "duplicate_user", e.getMessage());
            return "UserCreateView";
        }
    }

    @GetMapping("/{userId}/update")
    public String showEditUserPage(@PathVariable int userId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            UserDto userDto = userMapper.toDtoAsAdmin(userService.getUserById(userId));
            model.addAttribute("userId", userId);
            model.addAttribute("user", userDto);
            return "UserUpdateView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{userId}/update")
    public String updateUser(@PathVariable int userId,
                             @Valid @ModelAttribute("user") UserDto userDto,
                             BindingResult bindingResult,
                             HttpSession session,
                             Model model) {
        User admin;
        try {
            admin = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        if (bindingResult.hasErrors()) {
            return "UserUpdateView";
        }
        try {
            User updatedUser = userService.getUserById(userId);
            User user = userMapper.createUserTemp(updatedUser, userDto);
            userService.updateUser(user, admin);
            return "redirect:/users/{userId}";
        } catch (AuthorizationException e) {
        model.addAttribute("error", e.getMessage());
        return "AccessDeniedView";
    }
        catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (EntityDuplicateException e) {
            bindingResult.rejectValue("username", "duplicate_user", e.getMessage());
            return "UserUpdateView";
        }
    }

    @GetMapping("/{userId}/updateAsAdmin")
    public String showEditUserPageAsAdmin(@PathVariable int userId,
                                          HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            UserDto userDto = userMapper.toDtoAsAdmin(userService.getUserById(userId));
            model.addAttribute("userId", userId);
            model.addAttribute("user", userDto);
            model.addAttribute("userToUpdate", userService.getUserById(userId));
            return "UserUpdateViewAsAdmin";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{userId}/updateAsAdmin")
    public String updateUserAsAdmin(@PathVariable int userId, @Valid @ModelAttribute("user") UserDto userDto,
                                    BindingResult bindingResult, HttpSession session, Model model) {
        User admin;
        try {
            admin = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        if (bindingResult.hasErrors()) {
            return "UserUpdateViewAsAdmin";
        }
        try {
            User updatedUser = userService.getUserById(userId);
            User user = userMapper.dtoToObjectAsAdmin(updatedUser, userDto);
            model.addAttribute("admin", admin);
            userService.updateUser(user, admin);
            return "redirect:/users";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
        catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (EntityDuplicateException e) {
            bindingResult.rejectValue("username", "duplicate_user", e.getMessage());
            return "UserUpdateViewAsAdmin";
        }
    }

    @GetMapping("/{userId}/delete")
    public String showDeleteUserPage(@PathVariable int userId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            UserDto userDto = userMapper.toDto(userService.getUserById(userId));
            model.addAttribute("userId", userId);
            model.addAttribute("user", userDto);
            return "UserDeleteView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{userId}/delete")
    public String deleteUser(@PathVariable int userId, HttpSession session, Model model) {
        User authentication;
        try {
            authentication = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            userService.deleteUser(userId, authentication);
            session.removeAttribute("currentUser");
            return "UserDeletedSuccessView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
        catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{userId}/deleteAsAdmin")
    public String showDeleteUserAsAdminPage(@PathVariable int userId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            UserDto userDto = userMapper.toDto(userService.getUserById(userId));
            model.addAttribute("userId", userId);
            model.addAttribute("user", userDto);
            return "UserDeleteAsAdminView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{userId}/deleteAsAdmin")
    public String deleteAsAdminUser(@PathVariable int userId, HttpSession session, Model model) {
        User admin;
        try {
            admin = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            userService.deleteUser(userId, admin);
            return "redirect:/users";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
        catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }
}
