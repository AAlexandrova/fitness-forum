package com.example.fitnessforum.repositories;

import com.example.fitnessforum.exceptions.CustomException;
import com.example.fitnessforum.exceptions.EntityNotFoundException;
import com.example.fitnessforum.models.Like;
import com.example.fitnessforum.repositories.contracts.LikeRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.query.Query;
import java.util.List;

@Repository
public class LikeRepositoryImpl implements LikeRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public LikeRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Like> getLikes() {
        try(Session session = sessionFactory.openSession()){
            Query<Like> query = session.createQuery("from Like", Like.class);
            return query.list();
        }
    }

    @Override
    public Like getLikeById(int likeId) {
        try(Session session = sessionFactory.openSession()){
            Like like = session.get(Like.class, likeId);
            if(like == null){
                throw new EntityNotFoundException("Like", likeId);
            }
            return like;
        }
    }

    @Override
    public List<Like> getLikesByUserId(int userId) {
        try(Session session = sessionFactory.openSession()){
            Query<Like> query = session.createQuery("from Like where user.userId = :userId", Like.class);
            query.setParameter("userId", userId);
            List<Like> result = query.list();
            if(result.isEmpty()){
                throw new EntityNotFoundException("Likes", "userId", String.valueOf(userId));
            }
            return result;
        }
    }

    @Override
    public List<Like> getLikesByPostId(int postId) {
        try(Session session = sessionFactory.openSession()){
            Query<Like> query = session.createQuery("from Like where post.postId = :postId "
                    , Like.class);
            query.setParameter("postId", postId);
            List<Like> result = query.list();
            if(result.isEmpty()){
                throw new EntityNotFoundException("Likes", "postId", String.valueOf(postId));
            }
            return result;
        }
    }

    @Override
    public Like getLikeByPostAndUser(int postId, int userId) {
        try(Session session = sessionFactory.openSession()){
            Query<Like> query = session.createQuery("from Like " +
                            " where post.postId = :postId and user.userId = :userId "
                    , Like.class);
            query.setParameter("postId", postId);
            query.setParameter("userId", userId);
            List<Like> result = query.list();
            if(result.isEmpty()){
                throw new EntityNotFoundException(String.format("User with userId %d does not like/dislike post with postId %d"
                                , userId, postId));
            }
            return result.get(0);
        }
    }

    @Override
    public long getLikesCount(int postId) {
        try(Session session = sessionFactory.openSession()){
            Query<Long> query = session.createQuery("select count (likeId) from Like " +
                    " where upOrDown = true and post.postId = :postId ", Long.class);

            query.setParameter("postId", postId);
            List<Long> result = query.list();
            if(result.isEmpty()){return 0;}
            return result.get(0);
        }
    }

    @Override
    public long getDislikesCount(int postId) {
        try(Session session = sessionFactory.openSession()){
            Query<Long> query = session.createQuery("select count (likeId) from Like " +
                    " where upOrDown = false and post.postId = :postId ", Long.class);

            query.setParameter("postId", postId);
            List<Long> result = query.list();
            if(result.isEmpty()){return 0;}
            return result.get(0);
        }
    }


    @Override
    public void createLike(Like like) {
        try(Session session = sessionFactory.openSession()){
            session.save(like);
        }
    }

    @Override
    public void deleteLike(int likeId) {
        Like likeToDelete = getLikeById(likeId);
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.delete(likeToDelete);
            session.getTransaction().commit();
        }
    }

}
