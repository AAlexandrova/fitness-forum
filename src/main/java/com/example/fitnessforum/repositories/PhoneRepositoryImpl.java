package com.example.fitnessforum.repositories;

import com.example.fitnessforum.exceptions.EntityNotFoundException;
import com.example.fitnessforum.models.Phone;
import com.example.fitnessforum.models.User;
import com.example.fitnessforum.repositories.contracts.PhoneRepository;
import com.example.fitnessforum.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class PhoneRepositoryImpl implements PhoneRepository {

    private final SessionFactory sessionFactory;
    private final UserRepository userRepository;

    @Autowired
    public PhoneRepositoryImpl(SessionFactory sessionFactory, UserRepository userRepository) {
        this.sessionFactory = sessionFactory;
        this.userRepository = userRepository;
    }

    @Override
    public List<Phone> getPhones() {
        try (Session session = sessionFactory.openSession()) {
            Query<Phone> query = session.createQuery("from Phone", Phone.class);
            return query.list();
        }
    }

    @Override
    public Phone getPhoneById(int phoneId) {
        try (Session session = sessionFactory.openSession()) {
            Phone phone = session.get(Phone.class, phoneId);
            if (phone == null) {
                throw new EntityNotFoundException("Phone", phoneId);
            }
            return phone;
        }
    }

    @Override
    public Phone getPhoneByUserId(int userId) {
        try (Session session = sessionFactory.openSession()) {
            User user = userRepository.getUserById(userId);
            Query<Phone> queryList = session.createQuery(
                    "from Phone where user = :user ", Phone.class);
            queryList.setParameter("user", user);
            if (queryList.list().size() == 0) {
                throw new EntityNotFoundException("Phone", "User ID", Integer.toString(userId));
            }
            return queryList.list().get(0);
        }
    }

    @Override
    public List<Phone> filter(Optional<String> phone, Optional<Integer> userId, Optional<String> sort) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder(" from Phone ");
            Map<String, Object> queryParams = new HashMap<>();
            ArrayList<String> filter = new ArrayList<>();

            phone.ifPresent(value -> {
                filter.add(" phone like :phone ");
                queryParams.put("phone", "%" + value + "%");
            });

            userId.ifPresent(value -> {
                filter.add(" user.userId = :userId ");
                queryParams.put("userId", value);
            });

            if (!filter.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", filter));
            }

            sort.ifPresent(value -> queryString.append(generateStringFromSort(value)));

            Query<Phone> queryList = session.createQuery(queryString.toString(), Phone.class);
            queryList.setProperties(queryParams);
            return queryList.list();
        }
    }

    private String generateStringFromSort(String value) {
        StringBuilder queryString = new StringBuilder(" order by ");
        String[] params = value.split("_");
        if (params.length > 2) {
            throw new UnsupportedOperationException(
                    "Sort should have max two params divided by _ symbol!");
        }
        if (value.isEmpty()) {
            return "";
        }

        switch (params[0].toLowerCase()) {
            case "phoneid":
                queryString.append(" phoneId ");
                break;
            case "phone":
                queryString.append(" phone ");
                break;
            case "userid":
                queryString.append(" user.userId ");
                break;
            default:
                throw new UnsupportedOperationException(
                        "Sort should have max two params divided by _ symbol!");
        }
        if (params.length == 2 && params[1].equalsIgnoreCase("desc")) {
            queryString.append(" desc ");
        }
        return queryString.toString();
    }

    @Override
    public Phone createPhone(Phone phone) {
        try (Session session = sessionFactory.openSession()) {
            session.save(phone);
        }
        return phone;
    }

    @Override
    public Phone updatePhone(Phone phone) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(phone);
            session.getTransaction().commit();
        }
        return phone;
    }

    @Override
    public Phone deletePhone(int phoneId) {
        Phone phoneToDelete = getPhoneById(phoneId);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(phoneToDelete);
            session.getTransaction().commit();
        }
        return phoneToDelete;
    }
}
