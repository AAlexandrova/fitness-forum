package com.example.fitnessforum.repositories;

import com.example.fitnessforum.exceptions.EntityNotFoundException;
import com.example.fitnessforum.models.Tag;
import com.example.fitnessforum.repositories.contracts.TagRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.query.Query;

import java.util.List;

@Repository
public class TagRepositoryImpl implements TagRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public TagRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Tag> getTags() {
        try(Session session = sessionFactory.openSession()){
            Query<Tag> tags = session.createQuery("from Tag", Tag.class);
            return tags.list();
        }
    }

    @Override
    public void createTag(Tag tag) {
        try(Session session = sessionFactory.openSession()){
            session.save(tag);
        }
    }

    @Override
    public Tag getTagByTitle(String title) {
        try(Session session = sessionFactory.openSession()){
            Query<Tag> query = session.createQuery("from Tag where title = :title", Tag.class);
            query.setParameter("title", title);
            List<Tag> tags = query.list();
            if(tags.isEmpty()){
                throw new EntityNotFoundException("Tag", "title", title);
            }
            return tags.get(0);
        }
    }

    @Override
    public Tag getTagById(int tagId) {
        try(Session session = sessionFactory.openSession()){
            Tag tag = session.get(Tag.class, tagId);
            if(tag == null){
                throw new EntityNotFoundException("Tag", tagId);
            }
            return tag;
        }
    }

    @Override
    public void updateTag(Tag tag) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(tag);
            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteTag(int tagId) {
        Tag tagToDelete = getTagById(tagId);
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.delete(tagToDelete);
            session.getTransaction().commit();
        }
    }
}
