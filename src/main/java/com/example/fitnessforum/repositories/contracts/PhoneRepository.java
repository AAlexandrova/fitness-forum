package com.example.fitnessforum.repositories.contracts;

import com.example.fitnessforum.models.Phone;

import java.util.List;
import java.util.Optional;

public interface PhoneRepository {
    List<Phone> getPhones();

    Phone getPhoneById(int phoneId);

    Phone getPhoneByUserId(int userId);

    Phone createPhone(Phone phone);

    Phone updatePhone(Phone phone);

    Phone deletePhone(int phoneId);

    List<Phone> filter(Optional<String> phone, Optional<Integer> userId, Optional<String> sort);
}
