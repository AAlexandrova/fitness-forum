package com.example.fitnessforum.repositories.contracts;

import com.example.fitnessforum.models.Comment;
import com.example.fitnessforum.models.CommentFilterOptions;

import java.util.List;
import java.util.Optional;

public interface CommentRepository {
    List<Comment> getComments();

    Comment getCommentById(int commentId);

    Comment getCommentById(int commentId, int postId);

    List<Comment> getCommentByPostId(int postId);
    List<Comment> getCommentByPostId(int postId, CommentFilterOptions commentFilterOptions);

    Comment createComment(Comment comment);

    Comment updateComment(Comment comment);

    Comment deleteComment(int commentId);

    List<Comment> filter(Integer postId, Optional<Integer> commentId, Optional<Integer> userId, Optional<String> sort);
}
