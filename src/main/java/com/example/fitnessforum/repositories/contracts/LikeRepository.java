package com.example.fitnessforum.repositories.contracts;

import com.example.fitnessforum.models.Like;

import java.util.List;

public interface LikeRepository {
    List<Like> getLikes();

    Like getLikeById(int likeId);

    List<Like> getLikesByUserId(int userId);

    void createLike(Like like);

    void deleteLike(int likeId);

    List<Like> getLikesByPostId(int postId);

    Like getLikeByPostAndUser(int postId, int userId);

    long getLikesCount(int postId);

    long getDislikesCount(int postId);
}
