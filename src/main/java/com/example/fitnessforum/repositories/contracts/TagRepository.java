package com.example.fitnessforum.repositories.contracts;

import com.example.fitnessforum.models.Tag;

import java.util.List;

public interface TagRepository {
    List<Tag> getTags();

    void createTag(Tag tag);

    Tag getTagByTitle(String title);

    Tag getTagById(int tagId);

    void updateTag(Tag tag);

    void deleteTag(int tagId);
}
