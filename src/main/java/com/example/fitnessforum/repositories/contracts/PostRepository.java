package com.example.fitnessforum.repositories.contracts;

import com.example.fitnessforum.models.Post;
import com.example.fitnessforum.models.PostFilterOptions;

import java.util.List;

public interface PostRepository {
    List<Post> getPosts();
    List<Post> getPosts(PostFilterOptions filterOptions);
    Post getPostById(int postId);

    Post getPostByTitle(String title);

    void createPost(Post post);

    void updatePost(Post post);

    void deletePost(int postId);

    List<Post> getMostLikedPosts();

    List<Post> getTenMostCommentedPosts();

    List<Post> getTenMostRecentPosts();

    Long getPostsCount();
}
