package com.example.fitnessforum.repositories.contracts;

import com.example.fitnessforum.models.User;
import com.example.fitnessforum.models.UserFilterOptions;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    List<User> getUsers();

    List<User> getUsers(UserFilterOptions userFilterOptions);

    List<User> getUsersWithPosts();

    User getUserById(int id);

    User getUserByUsername(String username);

    User getUserByEmail(String email);

    User createUser(User user);

    User updateUser(User user);

    Long getUsersCount();

    List<User> filter(Optional<String> firstName, Optional<String> lastName, Optional<String> email,
                      Optional<Boolean> isAdmin, Optional<Boolean> isBlocked, Optional<String> username,
                      Optional<String> sort);
}