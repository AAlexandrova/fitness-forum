package com.example.fitnessforum.repositories;

import com.example.fitnessforum.exceptions.EntityNotFoundException;
import com.example.fitnessforum.models.Post;
import com.example.fitnessforum.models.PostFilterOptions;
import com.example.fitnessforum.repositories.contracts.PostRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import org.hibernate.query.Query;

import java.util.*;

@Repository
public class PostRepositoryImpl implements PostRepository {
    public static final String INVALID_SORT_PARAMS_MESSAGE = "SortBy should contain one sort parameter (title, content, userId or date) and (optional) sort order, divided by _ symbol.";
    private final SessionFactory sessionFactory;

    @Autowired
    public PostRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<Post> getPosts() {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery("from Post", Post.class);
            List<Post> posts = query.list();
            return posts;
        }
    }

    @Override
    public List<Post> getPosts(PostFilterOptions filterOptions) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder("select distinct p from Post p " +
                    " left join p.tags t ");
            Map<String, Object> queryParams = new HashMap<>();
            List<String> queryParts = new ArrayList<>();

            filterOptions.getTitle().ifPresent(value -> {
                queryParts.add(" p.title like :title ");
                queryParams.put("title", "%" + value + "%");
            });

            filterOptions.getContent().ifPresent(value -> {
                queryParts.add(" p.content like :content ");
                queryParams.put("content", "%" + value + "%");
            });

            filterOptions.getUserId().ifPresent(value -> {
                queryParts.add(" p.user.userId = :userId ");
                queryParams.put("userId", value);
            });

            filterOptions.getUsername().ifPresent(value -> {
                queryParts.add(" p.user.username like :username ");
                queryParams.put("username", "%" + value + "%");
            });

            filterOptions.getTag().ifPresent(value -> {
                queryParts.add(" t.title like :tagName ");
                queryParams.put("tagName", "%" + value + "%");
            });

            if (!queryParts.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", queryParts));
            }

            String sortString = generateSortString(filterOptions);
            if (sortString.equals("")) {
                queryString.append(" order by p.dateCreated desc ");
            } else {
                queryString.append(sortString);
            }

            Query<Post> query = session.createQuery(queryString.toString(), Post.class);
            query.setProperties(queryParams);

            return query.list();
        }
    }

    private String generateSortString(PostFilterOptions filterOptions) {
        if (filterOptions.getSortBy().isEmpty()) {
            return "";
        }

        StringBuilder queryString = new StringBuilder(" order by ");
        switch (filterOptions.getSortBy().get()) {
            case "title":
                queryString.append(" p.title ");
                break;
            case "content":
                queryString.append(" p.content ");
                break;
            case "username":
                queryString.append(" p.user.username ");
                break;
            case "date":
                queryString.append(" p.dateCreated ");
                break;
            default:
                return "";
        }

        if (filterOptions.getSortOrder().isPresent() &&
                filterOptions.getSortOrder().get().equalsIgnoreCase("desc")) {
            queryString.append(" desc ");
        }

        return queryString.toString();
    }

    @Override
    public Post getPostById(int postId) {
        try (Session session = sessionFactory.openSession()) {
            Post post = session.get(Post.class, postId);
            if (post == null) {
                throw new EntityNotFoundException("Post", postId);
            }
            return post;
        }
    }

    @Override
    public Post getPostByTitle(String title) {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery("from Post where title= :title", Post.class);
            query.setParameter("title", title);
            if (query.list().size() == 0) {
                throw new EntityNotFoundException("Post", "title", title);
            }
            return query.list().get(0);
        }
    }

    @Override
    public List<Post> getMostLikedPosts() {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery(
                    "select p from Like l " +
                            " join l.post p " +
                            " where l.upOrDown = true " +
                            " group by p " +
                            " order by count(p.postId) desc", Post.class);
            return query.list();
        }
    }

    @Override
    public List<Post> getTenMostCommentedPosts() {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery(
                    "select p from Comment c " +
                            " join c.post p " +
                            " group by p " +
                            " order by count(p.postId) desc", Post.class);
            query.setMaxResults(10);
            return query.list();
        }
    }

    @Override
    public List<Post> getTenMostRecentPosts() {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery(
                    "from Post " +
                            " order by dateCreated desc", Post.class);
            query.setMaxResults(10);
            return query.list();
        }
    }

    @Override
    public Long getPostsCount() {
        try (Session session = sessionFactory.openSession()) {
            Query<Long> query = session.createQuery(
                    "select count(*) from Post", Long.class);
            return query.uniqueResult();
        }
    }

    @Override
    public void createPost(Post post) {
        try (Session session = sessionFactory.openSession()) {
            session.save(post);
        }
    }

    @Override
    public void updatePost(Post post) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(post);
            session.getTransaction().commit();
        }
    }

    @Override
    public void deletePost(int postId) {
        Post postToDelete = getPostById(postId);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(postToDelete);
            session.getTransaction().commit();
        }
    }
}
