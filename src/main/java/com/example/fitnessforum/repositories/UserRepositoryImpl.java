package com.example.fitnessforum.repositories;

import com.example.fitnessforum.exceptions.EntityNotFoundException;
import com.example.fitnessforum.models.User;
import com.example.fitnessforum.models.UserFilterOptions;
import com.example.fitnessforum.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getUsers() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User", User.class);
            return query.list();
        }
    }

    @Override
    public List<User> getUsers(UserFilterOptions userFilterOptions) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder(" from User ");

            Map<String, Object> queryParams = new HashMap<>();
            ArrayList<String> filter = new ArrayList<>();

            userFilterOptions.getFirstName().ifPresent(v -> {
                filter.add(" firstName like :firstName ");
                queryParams.put("firstName", "%" + v + "%");
            });

            userFilterOptions.getLastName().ifPresent(v -> {
                filter.add(" lastName like :lastName ");
                queryParams.put("lastName", "%" + v + "%");
            });

            userFilterOptions.getEmail().ifPresent(v -> {
                filter.add(" email like :email ");
                queryParams.put("email", "%" + v + "%");
            });

            userFilterOptions.getIsAdmin().ifPresent(v -> {
                filter.add(" isAdmin = :isAdmin ");
                queryParams.put("isAdmin", Boolean.valueOf(v));
            });

            userFilterOptions.getIsBlocked().ifPresent(v -> {
                filter.add(" isBlocked = :isBlocked ");
                queryParams.put("isBlocked", Boolean.valueOf(v));
            });

            userFilterOptions.getUsername().ifPresent(v -> {
                filter.add(" username like :username ");
                queryParams.put("username", "%" + v + "%");
            });

            if (!filter.isEmpty()) {
                queryString
                        .append(" where ")
                        .append(String.join(" and ", filter));
            }
            queryString.append(generateOrderBy(userFilterOptions));

            Query<User> queryList = session.createQuery(queryString.toString(), User.class);
            queryList.setProperties(queryParams);
            return queryList.list();
        }
    }

    @Override
    public List<User> getUsersWithPosts() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session
                    .createQuery("select distinct user from Post", User.class);
            return query.list();
        }
    }

    @Override
    public User getUserById(int userId) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, userId);
            if (user == null) {
                throw new EntityNotFoundException("User", userId);
            }
            return user;
        }
    }

    @Override
    public User getUserByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username", username);

            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "username", username);
            }
            return result.get(0);
        }
    }

    @Override
    public User getUserByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email = :email", User.class);
            query.setParameter("email", email);

            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "email", email);
            }
            return result.get(0);
        }
    }

    @Override
    public User createUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.save(user);
        }
        return user;
    }

    @Override
    public User updateUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
        return user;
    }

    @Override
    public List<User> filter(Optional<String> firstName, Optional<String> lastName, Optional<String> email,
                             Optional<Boolean> isAdmin, Optional<Boolean> isBlocked, Optional<String> username,
                             Optional<String> sort) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder(" from User ");

            Map<String, Object> queryParams = new HashMap<>();
            ArrayList<String> filter = new ArrayList<>();

            firstName.ifPresent(v -> {
                filter.add(" firstName like :firstName ");
                queryParams.put("firstName", "%" + v + "%");
            });

            lastName.ifPresent(v -> {
                filter.add(" lastName like :lastName ");
                queryParams.put("lastName", "%" + v + "%");
            });

            email.ifPresent(v -> {
                filter.add(" email like :email ");
                queryParams.put("email", "%" + v + "%");
            });

            isAdmin.ifPresent(v -> {
                filter.add(" isAdmin = :isAdmin ");
                queryParams.put("isAdmin", Boolean.valueOf(v));
            });

            isBlocked.ifPresent(v -> {
                filter.add(" isBlocked = :isBlocked ");
                queryParams.put("isBlocked", Boolean.valueOf(v));
            });

            username.ifPresent(v -> {
                filter.add(" username like :username ");
                queryParams.put("username", "%" + v + "%");
            });

            if (!filter.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", filter));
            }

            sort.ifPresent(value -> queryString.append(generateStringFromSort(value)));

            Query<User> queryList = session.createQuery(queryString.toString(), User.class);
            queryList.setProperties(queryParams);
            return queryList.list();
        }
    }

    private String generateOrderBy(UserFilterOptions userFilterOptions) {
        if (userFilterOptions.getOrderBy().isEmpty()) {
            return "";
        }
        String sortBy = "";
        switch (userFilterOptions.getSortBy().get()) {
            case "userid":
                sortBy = "userId";
                break;
            case "firstName":
                sortBy = "firstName";
                break;
            case "lastName":
                sortBy = "lastName";
                break;
            case "email":
                sortBy = "email";
                break;
            case "isAdmin":
                sortBy = "isAdmin";
                break;
            case "isBlocked":
                sortBy = "isBlocked";
                break;
            case "username":
                sortBy = "username";
                break;
            default:
                return "";
        }

        sortBy = String.format(" order by %s", sortBy);
        if (userFilterOptions.getOrderBy().isPresent() && userFilterOptions.getOrderBy().get().equalsIgnoreCase("desc")) {
            sortBy = String.format(" %s desc", sortBy);
        }
        return sortBy;
    }


    private String generateStringFromSort(String value) {
        StringBuilder queryString = new StringBuilder(" order by ");
        String[] params = value.split("_");
        if (params.length > 2) {
            throw new UnsupportedOperationException(
                    "Sort should have max two params divided by _ symbol!");
        }
        if (value.isEmpty()) {
            return "";
        }

        switch (params[0].toLowerCase()) {
            case "userid":
                queryString.append(" userId ");
                break;
            case "firstname":
                queryString.append(" firstName ");
                break;
            case "lastname":
                queryString.append(" lastName ");
                break;
            case "email":
                queryString.append(" email ");
                break;
            case "isadmin":
                queryString.append(" isAdmin ");
                break;
            case "isblocked":
                queryString.append(" isBlocked ");
                break;
            case "username":
                queryString.append(" username ");
                break;
            default:
                throw new UnsupportedOperationException(
                        "Sort should have max two params divided by _ symbol!");
        }
        if (params.length == 2 && params[1].equalsIgnoreCase("desc")) {
            queryString.append(" desc ");
        }
        return queryString.toString();
    }

    @Override
    public Long getUsersCount() {
        try (Session session = sessionFactory.openSession()) {
            Query<Long> query = session.createQuery(
                    "select count(*) from User where username not like 'unknown'", Long.class);
            return query.uniqueResult();
        }
    }
}
