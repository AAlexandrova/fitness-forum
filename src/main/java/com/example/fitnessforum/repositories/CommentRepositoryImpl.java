package com.example.fitnessforum.repositories;

import com.example.fitnessforum.exceptions.EntityNotFoundException;
import com.example.fitnessforum.models.Comment;
import com.example.fitnessforum.models.CommentFilterOptions;
import com.example.fitnessforum.models.Post;
import com.example.fitnessforum.models.UserFilterOptions;
import com.example.fitnessforum.repositories.contracts.CommentRepository;
import com.example.fitnessforum.repositories.contracts.PostRepository;
import com.example.fitnessforum.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class CommentRepositoryImpl implements CommentRepository {
    private final UserRepository userRepository;
    private final PostRepository postRepository;
    private final SessionFactory sessionFactory;

    @Autowired
    public CommentRepositoryImpl(UserRepository userRepository,
                                 PostRepository postRepository,
                                 SessionFactory sessionFactory) {
        this.userRepository = userRepository;
        this.postRepository = postRepository;
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Comment> getComments() {
        try (Session session = sessionFactory.openSession()) {
            Query<Comment> query = session.createQuery("from Comment", Comment.class);
            return query.list();
        }
    }

    @Override
    public Comment getCommentById(int commentId) {
        try (Session session = sessionFactory.openSession()) {
            Comment comment = session.get(Comment.class, commentId);
            if (comment == null) {
                throw new EntityNotFoundException("Comment", commentId);
            }
            return comment;
        }
    }

    @Override
    public Comment getCommentById(int commentId, int postId) {
        try (Session session = sessionFactory.openSession()) {
            Post post = postRepository.getPostById(postId);
            Query<Comment> query = session.createQuery(
                    "from Comment where post = :post and commentId = :commentId", Comment.class);
            query.setParameter("post", post);
            query.setParameter("commentId", commentId);
            if (query.list().size() == 0) {
                throw new EntityNotFoundException("For this post: Comment", commentId);
            }
            return query.list().get(0);
        }
    }

    @Override
    public List<Comment> getCommentByPostId(int postId) {
        try (Session session = sessionFactory.openSession()) {
            Post post = postRepository.getPostById(postId);
            Query<Comment> queryList = session.createQuery(
                    "from Comment where post = :post order by commentId desc", Comment.class);
            queryList.setParameter("post", post);
            if (queryList.list().size() == 0) {
                throw new EntityNotFoundException("Comments for Post", postId);
            }
            return queryList.list();
        }
    }

    @Override
    public List<Comment> getCommentByPostId(int postId, CommentFilterOptions commentFilterOptions) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder(" select c from Comment c join c.user u ");
            Map<String, Object> queryParams = new HashMap<>();
            ArrayList<String> filter = new ArrayList<>();

            Post post = postRepository.getPostById(postId);

            filter.add(" c.post = :post ");
            queryParams.put("post", post);

            commentFilterOptions.getContent().ifPresent(v -> {
                filter.add(" c.content like :content ");
                queryParams.put("content", "%" + v + "%");
            });

            commentFilterOptions.getUsername().ifPresent(v -> {
                filter.add(" c.user.username like :username ");
                queryParams.put("username", "%" + v + "%");
            });

            if (!filter.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", filter));
            }
            if (filter.size() == 1) {
                queryString.append(" order by c.commentId desc ");
            }

            commentFilterOptions.getSortBy().ifPresent(value -> queryString
                    .append(generateOrderBy(commentFilterOptions)));
            Query<Comment> queryList = session.createQuery(queryString.toString(), Comment.class);

            queryList.setProperties(queryParams);
            return queryList.list();
        }
    }

    @Override
    public List<Comment> filter(Integer postId, Optional<Integer> commentId,
                                Optional<Integer> userId, Optional<String> sort) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder(" from Comment ");
            Map<String, Object> queryParams = new HashMap<>();
            ArrayList<String> filter = new ArrayList<>();

            filter.add(" post.postId = :postId ");
            queryParams.put("postId", postId);

            commentId.ifPresent(v -> {
                filter.add(" commentId = :commentId ");
                queryParams.put("commentId", v);
            });

            userId.ifPresent(v -> {
                filter.add(" user.userId = :userId ");
                queryParams.put("userId", v);
            });

            if (!filter.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", filter));
            }

            sort.ifPresent(value -> queryString.append(generateStringFromSort(value)));

            Query<Comment> queryList = session.createQuery(queryString.toString(), Comment.class);
            queryList.setProperties(queryParams);
            return queryList.list();
        }
    }

    private String generateStringFromSort(String value) {
        StringBuilder queryString = new StringBuilder(" order by ");
        String[] params = value.split("_");
        if (params.length > 2) {
            throw new UnsupportedOperationException("Sort should have max two params divided by _ symbol!");
        }
        if (value.isEmpty()) {
            return "";
        }

        switch (params[0].toLowerCase()) {
            case "commentid":
                queryString.append(" commentId ");
                break;
            case "userid":
                queryString.append(" user.userId ");
                break;
            default:
                throw new UnsupportedOperationException(
                        "Sort should have max two params divided by _ symbol!");
        }
        if (params.length == 2 && params[1].equalsIgnoreCase("desc")) {
            queryString.append(" desc ");
        }
        return queryString.toString();
    }


    private String generateOrderBy(CommentFilterOptions commentFilterOptions) {
        if (commentFilterOptions.getOrderBy().isEmpty()) {
            return "";
        }
        String sortBy = "";
        switch (commentFilterOptions.getSortBy().get()) {
            case "content":
                sortBy = "c.content";
                break;
            case "username":
                sortBy = "c.user.username";
                break;
            default:
                return "";
        }

        sortBy = String.format(" order by %s", sortBy);
        if (commentFilterOptions.getOrderBy().isPresent() && commentFilterOptions.getOrderBy().get().equalsIgnoreCase("desc")) {
            sortBy = String.format(" %s desc", sortBy);
        }

        return sortBy;
    }

    @Override
    public Comment createComment(Comment comment) {
        try (Session session = sessionFactory.openSession()) {
            session.save(comment);
        }
        return comment;
    }

    @Override
    public Comment updateComment(Comment comment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(comment);
            session.getTransaction().commit();
        }
        return comment;
    }

    @Override
    public Comment deleteComment(int commentId) {
        Comment commentToDelete = getCommentById(commentId);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(commentToDelete);
            session.getTransaction().commit();
        }
        return commentToDelete;
    }
}
