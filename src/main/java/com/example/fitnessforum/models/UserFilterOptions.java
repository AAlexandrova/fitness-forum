package com.example.fitnessforum.models;

import java.util.Optional;

public class UserFilterOptions {

    private Optional<String> firstName;
    private Optional<String> lastName;
    private Optional<String> email;
    private Optional<Boolean> isAdmin;
    private Optional<Boolean> isBlocked;
    private Optional<String> username;
    private Optional<String> sortBy;
    private Optional<String> orderBy;

    public UserFilterOptions() {
        this(null, null, null, null, null, null, null, null);
    }

    public UserFilterOptions(String firstName, String lastName, String email, Boolean isAdmin,
                             Boolean isBlocked, String username, String sortBy, String orderBy) {
        this.firstName = Optional.ofNullable(firstName);
        this.lastName = Optional.ofNullable(lastName);
        this.email = Optional.ofNullable(email);
        this.isAdmin = Optional.ofNullable(isAdmin);
        this.isBlocked = Optional.ofNullable(isBlocked);
        this.username = Optional.ofNullable(username);
        this.sortBy = Optional.ofNullable(sortBy);
        this.orderBy = Optional.ofNullable(orderBy);
    }

    public Optional<String> getFirstName() {
        return firstName;
    }

    public Optional<String> getLastName() {
        return lastName;
    }

    public Optional<String> getEmail() {
        return email;
    }

    public Optional<Boolean> getIsAdmin() {
        return isAdmin;
    }

    public Optional<Boolean> getIsBlocked() {
        return isBlocked;
    }

    public Optional<String> getUsername() {
        return username;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public Optional<String> getOrderBy() {
        return orderBy;
    }
}
