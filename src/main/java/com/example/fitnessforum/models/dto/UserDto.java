package com.example.fitnessforum.models.dto;

import jdk.jfr.BooleanFlag;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserDto {
    public static final int USER_FIRST_NAME_MIN_LENGTH = 4;
    public static final int USER_FIRST_NAME_MAX_LENGTH = 32;
    public static final int USER_LAST_NAME_MIN_LENGTH = 4;
    public static final int USER_LAST_NAME_MAX_LENGTH = 32;
    public static final int EMAIL_MIN_LENGTH = 4;
    public static final int EMAIL_MAX_LENGTH = 100;
    public static final int USERNAME_MIN_LENGTH = 4;
    public static final int USERNAME_MAX_LENGTH = 32;
    public static final int PASSWORD_MIN_LENGTH = 4;
    public static final int PASSWORD_MAX_LENGTH = 32;

    @NotNull(message = "First name can not be empty!")
    @Size(min = USER_FIRST_NAME_MIN_LENGTH, max = USER_FIRST_NAME_MAX_LENGTH,
            message = "First name should be between 4 and 32 symbols")
    private String firstName;

    @NotNull(message = "Last name can not be empty!")
    @Size(min = USER_LAST_NAME_MIN_LENGTH, max = USER_LAST_NAME_MAX_LENGTH,
            message = "Last name should be between 4 and 32 symbols")
    private String lastName;

    @NotNull(message = "E-mail can not be empty!")
    @Size(min = EMAIL_MIN_LENGTH, max = EMAIL_MAX_LENGTH,
            message = "E-mail should be between 2 and 100 symbols")
    private String email;

    @BooleanFlag
    private boolean isAdmin;

    @BooleanFlag
    private boolean isBlocked;

    @NotNull(message = "Username cannot be empty.")
    @Size(min = USERNAME_MIN_LENGTH, max = USERNAME_MAX_LENGTH,
            message = "Username should be between 4 and 32 symbols")
    private String username;

    @NotNull(message = "Password cannot be empty.")
    @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH,
            message = "Password should be between 4 and 32 symbols")
    private String password;

    public UserDto() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
