package com.example.fitnessforum.models.dto;

import com.example.fitnessforum.validation.constraints.StringLowerCase;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class TagDto {

    public static final int TAG_TITLE_MIN_LENGTH = 5;
    public static final int TAG_TITLE_MAX_LENGTH = 64;

    @NotNull(message = "Tag title cannot be null.")
    @Size(min = TAG_TITLE_MIN_LENGTH, max = TAG_TITLE_MAX_LENGTH, message = "Tag title should be between 5 and 64 symbols.")
    @StringLowerCase(message = "Tag title should be lowercase.")
    private String title;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
