package com.example.fitnessforum.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CommentDto {
    @NotNull(message = "Content can not be empty!")
    @Size(min = 2, max = 8192, message = "Content should be between 32 and 8192 symbols")
    private String content;

    public CommentDto() {
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
