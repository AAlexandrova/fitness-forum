package com.example.fitnessforum.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class LikeDto {

    @NotNull
    private boolean upOrDown;

    public boolean getUpOrDown() {
        return upOrDown;
    }

    public void setUpOrDown(boolean upOrDown) {
        this.upOrDown = upOrDown;
    }

}
