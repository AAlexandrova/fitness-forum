package com.example.fitnessforum.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class PhoneDtoOut {
    public static final int PHONE_MIN_LENGTH = 2;
    public static final int PHONE_MAX_LENGTH = 15;
    public static final int USER_FIRST_NAME_MIN_LENGTH = 4;
    public static final int USER_FIRST_NAME_MAX_LENGTH = 32;
    public static final int USER_LAST_NAME_MIN_LENGTH = 4;
    public static final int USER_LAST_NAME_MAX_LENGTH = 32;
    public static final int EMAIL_MIN_LENGTH = 4;
    public static final int EMAIL_MAX_LENGTH = 100;
    public static final int USERNAME_MIN_LENGTH = 4;
    public static final int USERNAME_MAX_LENGTH = 32;
    @Positive(message = "Phone Id should be positive number!")
    private int phoneId;
    @NotNull(message = "Phone number can not be empty!")
    @Size(min = PHONE_MIN_LENGTH, max = PHONE_MAX_LENGTH, message = "Content should be between 2 and 15 symbols")
    private String phone;
    @NotNull(message = "First name can not be empty!")
    @Size(min = USER_FIRST_NAME_MIN_LENGTH, max = USER_FIRST_NAME_MAX_LENGTH,
            message = "First name should be between 4 and 32 symbols")
    private String firstName;

    @NotNull(message = "Last name can not be empty!")
    @Size(min = USER_LAST_NAME_MIN_LENGTH, max = USER_LAST_NAME_MAX_LENGTH,
            message = "Last name should be between 4 and 32 symbols")
    private String lastName;

    @NotNull(message = "Username cannot be empty.")
    @Size(min = USERNAME_MIN_LENGTH, max = USERNAME_MAX_LENGTH,
            message = "Username should be between 4 and 32 symbols")
    private String username;

    @NotNull(message = "E-mail can not be empty!")
    @Size(min = EMAIL_MIN_LENGTH, max = EMAIL_MAX_LENGTH,
            message = "E-mail should be between 2 and 100 symbols")
    private String email;

    public PhoneDtoOut() {
    }

    public int getPhoneId() {
        return phoneId;
    }

    public void setPhoneId(int phoneId) {
        this.phoneId = phoneId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
