package com.example.fitnessforum.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PhoneDtoMvc {

    public static final int PHONE_MIN_LENGTH = 2;
    public static final int PHONE_MAX_LENGTH = 15;
    public static final int USERNAME_MIN_LENGTH = 4;
    public static final int USERNAME_MAX_LENGTH = 32;

    @NotNull(message = "Phone number can not be empty!")
    @Size(min = PHONE_MIN_LENGTH, max = PHONE_MAX_LENGTH, message = "Content should be between 2 and 15 symbols")
    private String phone;

    @NotNull(message = "Username cannot be empty.")
    @Size(min = USERNAME_MIN_LENGTH, max = USERNAME_MAX_LENGTH,
            message = "Username should be between 4 and 32 symbols")
    private String username;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
