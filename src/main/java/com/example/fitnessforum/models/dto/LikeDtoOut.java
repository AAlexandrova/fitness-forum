package com.example.fitnessforum.models.dto;

public class LikeDtoOut {
    private boolean upOrDown;

    private String username;

    public boolean isUpOrDown() {
        return upOrDown;
    }

    public void setUpOrDown(boolean upOrDown) {
        this.upOrDown = upOrDown;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
