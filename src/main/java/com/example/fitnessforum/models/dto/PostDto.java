package com.example.fitnessforum.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PostDto {
    public static final int POST_TITLE_MIN_LENGTH = 16;
    public static final int POST_TITLE_MAX_LENGTH = 64;
    public static final int POST_CONTENT_MIN_LENGTH = 32;
    public static final int POST_CONTENT_MAX_LENGTH = 8192;

    @NotNull(message = "Post title cannot be null.")
    @Size(min= POST_TITLE_MIN_LENGTH, max = POST_TITLE_MAX_LENGTH, message = "Post title should be between 16 and 24 symbols.")
    private String title;

    @NotNull(message = "Post content cannot be null.")
    @Size(min= POST_CONTENT_MIN_LENGTH, max = POST_CONTENT_MAX_LENGTH, message = "Post content should be between 32 and 8192 symbols.")
    private String content;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
