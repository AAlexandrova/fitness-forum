package com.example.fitnessforum.models.dto;

import java.util.ArrayList;
import java.util.List;

public class TagsCreateDto {
    private List<TagDto> inputTags;

    public TagsCreateDto() {
        this.inputTags = new ArrayList<>();
    }

    public void addTag(TagDto tagDto) {
        this.inputTags.add(tagDto);
    }

    public List<TagDto> getInputTags() {
        return inputTags;
    }

    public void setInputTags(List<TagDto> inputTags) {
        this.inputTags = inputTags;
    }
}
