package com.example.fitnessforum.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class CommentDtoOut {
    public static final int USERNAME_MIN_LENGTH = 4;
    public static final int USERNAME_MAX_LENGTH = 32;
    @Positive(message = "Comment Id should be positive number!")
    int commentId;
    @NotNull(message = "Content can not be empty!")
    @Size(min = 2, max = 8192, message = "Content should be between 32 and 8192 symbols")
    String content;
    @NotNull(message = "Username cannot be empty.")
    @Size(min = USERNAME_MIN_LENGTH, max = USERNAME_MAX_LENGTH,
            message = "Username should be between 4 and 32 symbols")
    String username;

    public CommentDtoOut() {
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
