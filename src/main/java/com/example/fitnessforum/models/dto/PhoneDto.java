package com.example.fitnessforum.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class PhoneDto {
    public static final int PHONE_MIN_LENGTH = 2;
    public static final int PHONE_MAX_LENGTH = 15;

    @NotNull(message = "Phone number can not be empty!")
    @Size(min = PHONE_MIN_LENGTH, max = PHONE_MAX_LENGTH, message = "Content should be between 2 and 15 symbols")
    private String phone;
    @Positive(message = "User id should be positive.")
    private int userId;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

}
