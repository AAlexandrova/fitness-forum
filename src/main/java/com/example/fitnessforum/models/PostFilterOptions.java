package com.example.fitnessforum.models;

import java.util.Optional;

public class PostFilterOptions {
    private Optional<String> title;
    private Optional<String> content;
    private Optional<Integer> userId;
    private Optional<String> username;
    private Optional<String> tag;
    private Optional<String> sortBy;
    private Optional<String> sortOrder;

    public PostFilterOptions(){
        this(null, null, null, null, null, null, null);
    }

    public PostFilterOptions(String title, String content, Integer userId, String username,
                             String tag, String sortBy, String sortOrder){

        this.title = Optional.ofNullable(title);
        this.content = Optional.ofNullable(content);
        this.userId = Optional.ofNullable(userId);
        this.username = Optional.ofNullable(username);
        this.tag = Optional.ofNullable(tag);
        this.sortBy = Optional.ofNullable(sortBy);
        this.sortOrder = Optional.ofNullable(sortOrder);
    }

    public Optional<String> getTag() {
        return tag;
    }

    public Optional<String> getTitle() {
        return title;
    }

    public Optional<String> getContent() {
        return content;
    }

    public Optional<Integer> getUserId() {
        return userId;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }

    public Optional<String> getUsername() {
        return username;
    }

    public void setTitle(Optional<String> title) {
        this.title = title;
    }

    public void setContent(Optional<String> content) {
        this.content = content;
    }

    public void setUserId(Optional<Integer> userId) {
        this.userId = userId;
    }

    public void setUsername(Optional<String> username) {
        this.username = username;
    }

    public void setTag(Optional<String> tag) {
        this.tag = tag;
    }

    public void setSortBy(Optional<String> sortBy) {
        this.sortBy = sortBy;
    }

    public void setSortOrder(Optional<String> sortOrder) {
        this.sortOrder = sortOrder;
    }
}
