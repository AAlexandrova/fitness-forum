package com.example.fitnessforum.models;

import java.util.Optional;

public class CommentFilterOptions {
    private Optional<String> username;
    private Optional<String> content;
    private Optional<String> sortBy;
    private Optional<String> orderBy;

    public CommentFilterOptions() {
        this(null, null, null, null);
    }

    public CommentFilterOptions(String username, String content, String sortBy, String orderBy) {
        this.username = Optional.ofNullable(username);
        this.content = Optional.ofNullable(content);
        this.sortBy = Optional.ofNullable(sortBy);
        this.orderBy = Optional.ofNullable(orderBy);
    }

    public Optional<String> getUsername() {
        return username;
    }

    public Optional<String> getContent() {
        return content;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public Optional<String> getOrderBy() {
        return orderBy;
    }
}
