package com.example.fitnessforum.helpers;

import com.example.fitnessforum.models.User;
import com.example.fitnessforum.models.dto.UserDto;
import com.example.fitnessforum.services.contracts.UserService;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    private final UserService userService;

    public UserMapper(UserService userService) {
        this.userService = userService;
    }

    public User dtoToObject(int userId, UserDto userDto) {
        User user = new User();
        user.setUserId(userId);

        user.setAdmin(false);
        user.setBlocked(false);

        createUserTemp(user, userDto);
        return user;
    }

    public User dtoToObject(UserDto userDto) {
        User user = new User();
        createUserTemp(user, userDto);
        return user;
    }

    public User createUserTemp(User user, UserDto userDto) {
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        return user;
    }

    public User dtoToObjectAsUser(int userId, UserDto userDto) {
        User user = userService.getUserById(userId);
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        return user;
    }

    public UserDto toDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setEmail(user.getEmail());
        userDto.setUsername(user.getUsername());
        userDto.setPassword(user.getPassword());
        return userDto;
    }

    public UserDto toDtoAsAdmin(User user) {
        UserDto userDto = new UserDto();
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setEmail(user.getEmail());
        userDto.setUsername(user.getUsername());
        userDto.setPassword(user.getPassword());
        userDto.setAdmin(user.isAdmin());
        userDto.setBlocked(user.isBlocked());
        return userDto;
    }

    public User dtoToObjectAsAdmin(User user, UserDto userDto) {
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setAdmin(userDto.isAdmin());
        user.setBlocked(userDto.isBlocked());
        return user;
    }
}
