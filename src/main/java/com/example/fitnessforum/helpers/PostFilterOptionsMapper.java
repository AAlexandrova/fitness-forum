package com.example.fitnessforum.helpers;

import com.example.fitnessforum.models.PostFilterOptions;
import com.example.fitnessforum.models.dto.PostFilterDto;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PostFilterOptionsMapper {

    public PostFilterOptions dtoToObject(PostFilterDto filterDto){
        PostFilterOptions filterOptions = new PostFilterOptions();

        if (filterDto.getTitle()!=null && filterDto.getTitle().isEmpty()) {
            filterOptions.setTitle(Optional.empty());
        }
        else{filterOptions.setTitle(Optional.ofNullable(filterDto.getTitle()));}

        if (filterDto.getContent()!=null && filterDto.getContent().isEmpty()) {
            filterOptions.setContent(Optional.empty());
        }
        else{filterOptions.setContent(Optional.ofNullable(filterDto.getContent()));}

        filterOptions.setUserId(Optional.ofNullable(filterDto.getUserId()));

        if (filterDto.getUsername()!=null && filterDto.getUsername().isEmpty()) {
            filterOptions.setUsername(Optional.empty());
        }
        else{filterOptions.setUsername(Optional.ofNullable(filterDto.getUsername()));}

        if (filterDto.getTag()!=null && filterDto.getTag().isEmpty()) {
            filterOptions.setTag(Optional.empty());
        }
        else{filterOptions.setTag(Optional.ofNullable(filterDto.getTag()));}


        filterOptions.setSortBy(Optional.ofNullable(filterDto.getSortBy()));
        filterOptions.setSortOrder(Optional.ofNullable(filterDto.getSortOrder()));
        return filterOptions;
    }
}
