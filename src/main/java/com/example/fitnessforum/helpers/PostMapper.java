package com.example.fitnessforum.helpers;

import com.example.fitnessforum.models.Post;
import com.example.fitnessforum.models.Tag;
import com.example.fitnessforum.models.dto.PostDto;
import com.example.fitnessforum.models.dto.PostDtoOut;
import com.example.fitnessforum.services.contracts.LikeService;
import com.example.fitnessforum.services.contracts.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;

@Component
public class PostMapper {

    private final PostService postService;
    private final LikeService likeService;

    @Autowired
    public PostMapper(PostService postService, LikeService likeService) {
        this.postService = postService;
        this.likeService = likeService;
    }

    public Post dtoToPost(int postId, PostDto postDto) {
        Post post = dtoToPost(postDto);
        post.setPostId(postId);
        Post existingPost = postService.getPostById(postId);
        post.setUser(existingPost.getUser());
        return post;
    }

    public Post dtoToPost(PostDto postDto) {
        Post post = new Post();
        post.setTitle(postDto.getTitle());
        post.setContent(postDto.getContent());
        return post;
    }

    public Post dtoToPostMvc(Post post, PostDto postDto) {
        post.setTitle(postDto.getTitle());
        post.setContent(postDto.getContent());
        return post;
    }

    public PostDto postToDto(Post post) {
        PostDto postDto = new PostDto();
        postDto.setTitle(post.getTitle());
        postDto.setContent(post.getContent());
        return postDto;
    }

    public PostDtoOut postToDtoOut(Post post) {
        PostDtoOut postDtoOut = new PostDtoOut();
        postDtoOut.setPostId(post.getPostId());
        postDtoOut.setTitle(post.getTitle());
        postDtoOut.setContent(post.getContent());
        postDtoOut.setUsername(post.getUser().getUsername());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        postDtoOut.setDateCreated(formatter.format(post.getDateCreated()));
        postDtoOut.setLikesCount(likeService.getLikesCount(post.getPostId()));
        postDtoOut.setDislikesCount(likeService.getDislikesCount(post.getPostId()));
        if (!post.getTags().isEmpty()) {
            for (Tag tag : post.getTags()) {
                postDtoOut.getTags().add(tag.getTitle());
            }
        }
        return postDtoOut;
    }
}
