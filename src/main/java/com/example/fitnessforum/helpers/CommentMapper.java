package com.example.fitnessforum.helpers;

import com.example.fitnessforum.models.Comment;
import com.example.fitnessforum.models.dto.CommentDto;
import com.example.fitnessforum.models.dto.CommentDtoOut;
import com.example.fitnessforum.models.dto.CommentDtoOutForAdmin;
import com.example.fitnessforum.services.contracts.CommentService;
import com.example.fitnessforum.services.contracts.PostService;
import com.example.fitnessforum.services.contracts.UserService;
import org.springframework.stereotype.Component;

@Component
public class CommentMapper {
    private final UserService userService;
    private final PostService postService;
    private final CommentService commentService;

    public CommentMapper(UserService userService, PostService postService, CommentService commentService) {
        this.userService = userService;
        this.postService = postService;
        this.commentService = commentService;
    }

    public Comment dtoToObject(int postId, int userId, CommentDto commentDto) {
        Comment comment = new Comment();
        comment.setContent(commentDto.getContent());
        comment.setUser(userService.getUserById(userId));
        comment.setPost(postService.getPostById(postId));
        return comment;
    }


    public Comment dtoToObject(Comment comment, CommentDto commentDto) {
        comment.setContent(commentDto.getContent());
        return comment;
    }

    public CommentDtoOut commentDtoOut(Comment comment, CommentDtoOut commentDtoOut) {
        commentDtoOut.setCommentId(comment.getCommentId());
        commentDtoOut.setContent(comment.getContent());
        commentDtoOut.setUsername(comment.getUser().getUsername());
        return commentDtoOut;
    }

    public CommentDto commentDtoMvc(Comment comment) {
        CommentDto commentDto = new CommentDto();
        commentDto.setContent(comment.getContent());
        return commentDto;
    }


    public CommentDtoOutForAdmin commentDtoOutForAdmin(Comment comment, CommentDtoOutForAdmin commentDtoOutForAdmin) {
        commentDtoOutForAdmin.setCommentId(comment.getCommentId());
        commentDtoOutForAdmin.setContent(comment.getContent());
        commentDtoOutForAdmin.setUserId(comment.getUser().getUserId());
        commentDtoOutForAdmin.setUsername(comment.getUser().getUsername());
        commentDtoOutForAdmin.setPostId(comment.getPost().getPostId());
        return commentDtoOutForAdmin;
    }
}
