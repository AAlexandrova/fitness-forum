package com.example.fitnessforum.helpers;

import com.example.fitnessforum.models.Tag;
import com.example.fitnessforum.models.dto.TagDto;
import com.example.fitnessforum.repositories.contracts.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TagMapper {
    private final TagRepository tagRepository;

    @Autowired
    public TagMapper(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    public Tag dtoToTag(TagDto tagDto){
        Tag tag = new Tag();
        tag.setTitle(tagDto.getTitle());
        return tag;
    }

    //за ъпдейт:
    public Tag dtoToTag(int tagId, TagDto tagDto){
        Tag tag = tagRepository.getTagById(tagId);
        tag.setTitle(tagDto.getTitle());
        return tag;
    }

    public TagDto tagToDto(Tag tag){
        TagDto tagDto = new TagDto();
        tagDto.setTitle(tag.getTitle());
        return tagDto;
    }
}
