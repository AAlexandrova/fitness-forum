package com.example.fitnessforum.helpers;

import com.example.fitnessforum.models.Like;
import com.example.fitnessforum.models.dto.LikeDto;
import com.example.fitnessforum.models.Post;
import com.example.fitnessforum.models.User;
import com.example.fitnessforum.models.dto.LikeDtoOut;
import com.example.fitnessforum.services.contracts.PostService;
import com.example.fitnessforum.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LikeMapper {

    private final UserService userService;
    private final PostService postService;

    @Autowired
    public LikeMapper(UserService userService, PostService postService) {
        this.userService = userService;
        this.postService = postService;
    }

    public Like dtoToLike(LikeDto likeDto, User user, Post post){
        Like like = new Like();
        like.setUpOrDown(likeDto.getUpOrDown());
        like.setUser(user);
        like.setPost(post);
        return like;
    }

    public LikeDtoOut likeToDtoOut(Like like){
        LikeDtoOut likeDtoOut = new LikeDtoOut();
        likeDtoOut.setUpOrDown(like.getUpOrDown());
        likeDtoOut.setUsername(like.getUser().getUsername());
        return likeDtoOut;
    }
}
