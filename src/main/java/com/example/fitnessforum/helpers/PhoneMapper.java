package com.example.fitnessforum.helpers;

import com.example.fitnessforum.models.Phone;
import com.example.fitnessforum.models.dto.PhoneDto;
import com.example.fitnessforum.models.dto.PhoneDtoMvc;
import com.example.fitnessforum.models.dto.PhoneDtoOut;
import com.example.fitnessforum.services.contracts.UserService;
import org.springframework.stereotype.Component;

@Component
public class PhoneMapper {
    private final UserService userService;

    public PhoneMapper(UserService userService) {
        this.userService = userService;
    }

    public Phone dtoToObject(int phoneId, PhoneDto phoneDto) {
        Phone phone = new Phone();
        phone.setPhoneId(phoneId);
        return dtoToObject(phone, phoneDto);
    }

    public Phone dtoToObject(Phone phone, PhoneDto phoneDto) {
        phone.setPhone(phoneDto.getPhone());
        phone.setUser(userService.getUserById(phoneDto.getUserId()));
        return phone;
    }

    public Phone dtoToObjectMvc(Phone phone, PhoneDtoMvc phoneDtoMvc) {
        phone.setPhone(phoneDtoMvc.getPhone());
        phone.setUser(userService.getUserByUsername(phoneDtoMvc.getUsername()));
        return phone;
    }

    public Phone dtoToObjectForCreate(PhoneDtoMvc phoneDtoMvc) {
        Phone phone = new Phone();
        phone.setPhone(phoneDtoMvc.getPhone());
        phone.setUser(userService.getUserByUsername(phoneDtoMvc.getUsername()));
        return phone;
    }

    public PhoneDtoMvc toDto(Phone phone) {
        PhoneDtoMvc phoneDtoMvc = new PhoneDtoMvc();
        phoneDtoMvc.setPhone(phone.getPhone());
        phoneDtoMvc.setUsername(phone.getUser().getUsername());
        return phoneDtoMvc;
    }

    public PhoneDtoOut phoneDtoOut(Phone phone, PhoneDtoOut phoneDtoOut) {
        phoneDtoOut.setPhoneId(phone.getPhoneId());
        phoneDtoOut.setPhone(phone.getPhone());
        phoneDtoOut.setFirstName(phone.getUser().getFirstName());
        phoneDtoOut.setLastName(phone.getUser().getLastName());
        phoneDtoOut.setUsername(phone.getUser().getUsername());
        phoneDtoOut.setEmail(phone.getUser().getEmail());
        return phoneDtoOut;
    }
}
