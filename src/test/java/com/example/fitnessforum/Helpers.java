package com.example.fitnessforum;

import com.example.fitnessforum.models.*;

import java.time.LocalDateTime;
import java.util.HashSet;

public class Helpers {
    public static User createMockUser() {
        var mockUser = new User();
        mockUser.setUserId(1);
        mockUser.setFirstName("MockFirstName");
        mockUser.setLastName("MockLastName");
        mockUser.setEmail("mock@user.com");
        mockUser.setUsername("MockUsername");
        mockUser.setPassword("MockPassword");
        return mockUser;
    }

    public static User createMockAdmin() {
        User mockUser = createMockUser();
        mockUser.setAdmin(true);
        return mockUser;
    }

    public static User createMockBlocked() {
        User mockUser = createMockUser();
        mockUser.setBlocked(true);
        return mockUser;
    }

    public static Post createMockPost(){
        Post mockPost = new Post();
        mockPost.setPostId(1);
        mockPost.setTitle("MockPostTitle");
        mockPost.setContent("MockContent");
        mockPost.setUser(createMockUser());
        mockPost.setDateCreated(LocalDateTime.now());
        mockPost.setTags(new HashSet<>());
        mockPost.getTags().add(createMockTag());
        Tag anotherTag = createMockTag();
        anotherTag.setTagId(2);
        anotherTag.setTitle("MockTagTitle2");
        mockPost.getTags().add(anotherTag);
        return mockPost;
    }

    public static Tag createMockTag(){
        Tag mockTag = new Tag();
        mockTag.setTagId(1);
        mockTag.setTitle("MockTagTitle");
        return mockTag;
    }

    public static Comment createMockComment(){
        Comment mockComment = new Comment();
        mockComment.setCommentId(1);
        mockComment.setContent("MockCommentContent");
        mockComment.setUser(createMockUser());
        mockComment.setPost(createMockPost());
        return mockComment;
    }

    public static Phone createMockPhone(){
        Phone mockPhone = new Phone();
        mockPhone.setPhoneId(1);
        mockPhone.setPhone("MockPhone");
        mockPhone.setUser(createMockAdmin());
        return mockPhone;
    }

    public static Like createMockLike(){
        Like mockLike = new Like();
        mockLike.setLikeId(1);
        mockLike.setUpOrDown(true);
        mockLike.setUser(createMockUser());
        mockLike.setPost(createMockPost());
        return mockLike;
    }

    public static Like createMockDislike(){
        Like mockDislike = createMockLike();
        mockDislike.setUpOrDown(false);
        return mockDislike;
    }

    public static PostFilterOptions createMockPostFilterOptions(){
        return new PostFilterOptions("MockPostTitle", "MockPostContent",
                1, "username", "date", "tag", "sortBy", "sortOder");
    }
}
