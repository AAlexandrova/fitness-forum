package com.example.fitnessforum.services;

import com.example.fitnessforum.exceptions.AuthorizationException;
import com.example.fitnessforum.exceptions.CustomException;
import com.example.fitnessforum.exceptions.EntityNotFoundException;
import com.example.fitnessforum.models.Like;
import com.example.fitnessforum.models.Post;
import com.example.fitnessforum.repositories.contracts.LikeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import com.example.fitnessforum.models.User;

import static com.example.fitnessforum.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class LikeServiceImplTests {
    @Mock
    LikeRepository mockLikeRepository;

    @InjectMocks
    LikeServiceImpl likeService;

    @Test
    public void getLikesByPostId_Should_CallRepository(){
        //Act
        likeService.getLikesByPostId(Mockito.anyInt());
        //Assert
        Mockito.verify(mockLikeRepository, Mockito.times(1))
                .getLikesByPostId(Mockito.anyInt());
    }

    @Test
    public void getLikeById_Should_ThrowException_When_UserNotAdmin(){
        //Arrange
        User user = createMockUser();

        //Act, Assert
        Assertions.assertThrows(AuthorizationException.class
                , ()->likeService.getLikeById(1, user));
    }

    @Test
    public void getLikeById_Should_CallRepository_When_UserIsAdmin(){
        //Arrange
        User admin = createMockAdmin();
        //Act
        likeService.getLikeById(1, admin);
        //Assert
        Mockito.verify(mockLikeRepository, Mockito.times(1))
                .getLikeById(Mockito.anyInt());
    }

    @Test
    public void createLike_Should_ThrowException_When_UserBlocked(){
        //Arrange
        Like like = createMockLike();
        like.getUser().setBlocked(true);

        //Act, Assert
        Assertions.assertThrows(AuthorizationException.class
                , ()-> likeService.createLike(like));
    }

    @Test
    public void createLike_Should_ThrowException_When_UserAlreadyLikesThisPost(){
        //Arrange
        Like like = createMockLike();
        Mockito.when(mockLikeRepository.getLikeByPostAndUser(Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(like);

        //Act, Assert
        Assertions.assertThrows(CustomException.class, ()-> likeService.createLike(like));
    }

    @Test
    public void createLike_Should_ThrowException_When_UserAttemptsToLikeHisOwnPost(){
        //Arrange
        Like like = createMockLike();
        Mockito.when(mockLikeRepository.getLikeByPostAndUser(Mockito.anyInt(), Mockito.anyInt()))
                .thenThrow(CustomException.class);
        like.setUser(like.getPost().getUser());

        //Act, Assert
        Assertions.assertThrows(CustomException.class, ()-> likeService.createLike(like));
    }

    @Test
    public void createLike_Should_CallRepository(){
        //Arrange
        Like like = createMockLike();
        Mockito.when(mockLikeRepository.getLikeByPostAndUser(Mockito.anyInt(), Mockito.anyInt()))
                .thenThrow(EntityNotFoundException.class);
        User newUser = createMockUser();
        newUser.setUserId(2);
        newUser.setUsername("new user");
        like.setUser(newUser);

        //Act
        likeService.createLike(like);

        //Assert
        Mockito.verify(mockLikeRepository, Mockito.times(1))
                .createLike(like);
    }

    @Test
    public void deleteLike_Should_ThrowException_When_UserBlocked(){
        //Arrange
        Like like = createMockLike();
        like.getUser().setBlocked(true);
        Mockito.when(mockLikeRepository.getLikeById(Mockito.anyInt()))
                .thenReturn(like);

        //Act, Assert
        Assertions.assertThrows(AuthorizationException.class
                , ()-> likeService.deleteLike(like.getLikeId()));
    }

    @Test
    public void deleteLike_Should_CallRepository(){
        //Arrange
        Like like = createMockLike();
        Mockito.when(mockLikeRepository.getLikeById(Mockito.anyInt()))
                .thenReturn(like);

        //Act
        likeService.deleteLike(like.getLikeId());

        //Assert
        Mockito.verify(mockLikeRepository, Mockito.times(1))
                .deleteLike(like.getLikeId());
    }

    @Test
    public void getLikeByPostAndUser_Should_CallRepository(){
        //Arrange
        Post post = createMockPost();
        User user = createMockUser();
        //Act
        likeService.getLikeByPostAndUser(post, user);
        //Assert
        Mockito.verify(mockLikeRepository, Mockito.times(1))
                .getLikeByPostAndUser(post.getPostId(), user.getUserId());
    }

    @Test
    public void getLikesCount_Should_CallRepository(){
        //Arrange
        Post post = createMockPost();
        //Act
        likeService.getLikesCount(post.getPostId());
        //Assert
        Mockito.verify(mockLikeRepository, Mockito.times(1))
                .getLikesCount(post.getPostId());
    }

    @Test
    public void getDislikesCount_Should_CallRepository(){
        //Arrange
        Post post = createMockPost();
        //Act
        likeService.getDislikesCount(post.getPostId());
        //Assert
        Mockito.verify(mockLikeRepository, Mockito.times(1))
                .getDislikesCount(post.getPostId());
    }
}
