package com.example.fitnessforum.services;

import com.example.fitnessforum.exceptions.AuthorizationException;
import com.example.fitnessforum.exceptions.EntityDuplicateException;
import com.example.fitnessforum.exceptions.EntityNotFoundException;
import com.example.fitnessforum.models.User;
import com.example.fitnessforum.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.example.fitnessforum.Helpers.*;
import static com.example.fitnessforum.Helpers.createMockUser;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {
    @Mock
    UserRepository mockUserRepository;

    @InjectMocks
    UserServiceImpl userService;

    @Test
    void getUsers_Should_CallRepository() {
        // Arrange
        Mockito.when(mockUserRepository.getUsers()).thenReturn(null);
        // Act
        userService.getUsers();
        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getUsers();
    }

    @Test
    public void getUsers_Should_ReturnUser_When_MatchByIdExist() {
        // Arrange
        User mockUser = createMockUser();
        Mockito.when(mockUserRepository.getUserById(Mockito.anyInt()))
                .thenReturn(mockUser);
        // Act
        User result = userService.getUserById(mockUser.getUserId());

        // Assert
        Assertions.assertEquals(mockUser, result);
    }

    @Test
    public void getUsers_Should_ReturnUser_When_MatchByUsernameExist() {
        // Arrange
        User mockUser = createMockUser();
        Mockito.when(mockUserRepository.getUserByUsername(Mockito.anyString()))
                .thenReturn(mockUser);
        // Act
        User result = userService.getUserByUsername(mockUser.getUsername());

        // Assert
        Assertions.assertEquals(mockUser, result);
    }

    @Test
    void getWithFilter_Should_CallRepository() {
        // Arrange
        Mockito.when(mockUserRepository.filter(Optional.empty(), Optional.empty(),
                        Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(),
                        Optional.empty()))
                .thenReturn(null);

        // Act
        userService.filter(Optional.empty(), Optional.empty(),
                Optional.empty(), Optional.empty(), Optional.empty(),
                Optional.empty(), Optional.empty());

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .filter(Optional.empty(), Optional.empty(),
                        Optional.empty(), Optional.empty(), Optional.empty(),
                        Optional.empty(), Optional.empty());
    }

    @Test
    public void create_Should_CallRepository_When_UserWithSameNameAndEmailDoesNotExist() {
        // Arrange
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getUserByUsername(mockUser.getUsername()))
                .thenThrow(EntityNotFoundException.class);
        Mockito.when(mockUserRepository.getUserByEmail(mockUser.getEmail()))
                .thenThrow(EntityNotFoundException.class);
        // Act
        userService.createUser(mockUser);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .createUser(mockUser);
    }

    @Test
    public void create_Should_Throw_When_UserWithSameNameExists() {
        // Arrange
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getUserByUsername(mockUser.getUsername()))
                .thenReturn(mockUser);

        // Act, Assert
        Assertions.assertThrows(
                EntityDuplicateException.class,
                () -> userService.createUser(mockUser));
    }

    @Test
    public void create_Should_Throw_When_UserWithSameEmailExists() {
        // Arrange
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getUserByEmail(mockUser.getEmail()))
                .thenReturn(mockUser);

        // Act, Assert
        Assertions.assertThrows(
                EntityDuplicateException.class,
                () -> userService.createUser(mockUser));
    }

    @Test
    void update_Should_CallRepository_When_UserIsCreator() {
        // Arrange
        User mockUser = createMockUser();
        User mockUserCreator = createMockUser();

        Mockito.when(mockUserRepository.getUserByUsername(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockUserRepository.getUserByEmail(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        userService.updateUser(mockUser, mockUserCreator);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .updateUser(mockUser);
    }

    @Test
    void update_Should_CallRepository_When_UserIsAdmin() {
        // Arrange
        User mockUser = createMockUser();
        User mockUserAdmin = createMockAdmin();

        Mockito.when(mockUserRepository.getUserByUsername(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockUserRepository.getUserByEmail(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        userService.updateUser(mockUser, mockUserAdmin);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .updateUser(mockUser);
    }

    @Test
    public void update_Should_ThrowException_When_UserIsNotCreatorOrAdmin() {
        // Arrange
        User mockUser = createMockUser();

        // Act, Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> userService.updateUser(mockUser, Mockito.mock(User.class)));
    }

    @Test
    public void update_Should_ThrowException_When_UserIsBlocked() {
        // Arrange
        User mockUser = createMockUser();
        User blocked = createMockBlocked();

        // Act, Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> userService.updateUser(mockUser, blocked));
    }

    @Test
    public void update_Should_ThrowException_When_UserNameIsTaken() {
        // Arrange
        User mockUser = createMockUser();
        User mockUserCreator = createMockUser();

        User mockExistingUserWithSameName = createMockUser();
        mockExistingUserWithSameName.setUserId(2);

        Mockito.when(mockUserRepository.getUserByUsername(Mockito.anyString()))
                .thenReturn(mockExistingUserWithSameName);

        // Act, Assert
        Assertions.assertThrows(
                EntityDuplicateException.class,
                () -> userService.updateUser(mockUser, mockUserCreator));
    }

    @Test
    void delete_Should_CallRepository_When_UserIsCreator() {
        // Arrange
        User mockUser = createMockUser();
        User mockUserCreator = createMockUser();

        Mockito.when(mockUserRepository.getUserById(Mockito.anyInt()))
                .thenReturn(mockUser);

        // Act
        userService.deleteUser(1, mockUserCreator);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .updateUser(mockUser);
    }

    @Test
    void delete_Should_CallRepository_When_UserIsAdmin() {
        // Arrange
        User mockUser = createMockUser();
        User mockUserAdmin = createMockAdmin();

        Mockito.when(mockUserRepository.getUserById(Mockito.anyInt()))
                .thenReturn(mockUser);

        // Act
        userService.deleteUser(1, mockUserAdmin);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .updateUser(mockUser);
    }

    @Test
    void delete_Should_ThrowException_When_UserIsNotAdminOrCreator() {
        // Arrange
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getUserById(Mockito.anyInt()))
                .thenReturn(mockUser);

        User mockUserNotAdminOrCreator = Mockito.mock(User.class);

        // Act, Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> userService.deleteUser(1, mockUserNotAdminOrCreator));
    }

}
