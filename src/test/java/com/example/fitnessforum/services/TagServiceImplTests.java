package com.example.fitnessforum.services;

import com.example.fitnessforum.exceptions.AuthorizationException;
import com.example.fitnessforum.exceptions.EntityDuplicateException;
import com.example.fitnessforum.exceptions.EntityNotFoundException;
import com.example.fitnessforum.models.Post;
import com.example.fitnessforum.models.Tag;
import com.example.fitnessforum.models.User;
import com.example.fitnessforum.repositories.contracts.PostRepository;
import com.example.fitnessforum.repositories.contracts.TagRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static com.example.fitnessforum.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class TagServiceImplTests {
    @Mock
    TagRepository mockTagRepository;

    @Mock
    PostRepository mockPostRepository;

    @InjectMocks
    TagServiceImpl tagService;

    @Test
    public void getTags_Should_CallRepository_When_UserIsAdmin(){
        //Arrange
        User admin = createMockAdmin();

        //Act
        tagService.getTags(admin);

        //Assert
        Mockito.verify(mockTagRepository, Mockito.times(1)).getTags();
    }

    @Test
    public void getTags_Should_ThrowException_When_UserIsNotAdmin(){
        //Arrange
        User user = createMockUser();

        //Act, Assert
        Assertions.assertThrows(AuthorizationException.class
                , ()->tagService.getTags(user));
    }

    @Test
    public void getTagById_Should_CallRepository(){
        //Arrange
        Tag tag = createMockTag();
        //Act
        tagService.getTagById(tag.getTagId());

        //Assert
        Mockito.verify(mockTagRepository, Mockito.times(1)).getTagById(tag.getTagId());
    }

    @Test
    public void getTagById_Should_CallRepository_When_UserIsAdmin(){
        //Arrange
        User admin = createMockAdmin();
        Tag tag = createMockTag();

        //Act
        tagService.getTagById(admin, tag.getTagId());

        //Assert
        Mockito.verify(mockTagRepository, Mockito.times(1)).getTagById(tag.getTagId());
    }

    @Test
    public void getTagById_Should_ThrowException_When_UserIsNotAdmin(){
        //Arrange
        User user = createMockUser();
        Tag tag = createMockTag();

        //Act, Assert
        Assertions.assertThrows(AuthorizationException.class
                , ()->tagService.getTagById(user, tag.getTagId()));
    }

    @Test
    public void getOrCreateTag_Should_ThrowException_When_UserBlocked(){
        //Arrange
        User user = createMockBlocked();
        Tag tag = createMockTag();

        //Act, Assert
        Assertions.assertThrows(AuthorizationException.class
                , ()->tagService.getOrCreateTag(user, tag));
    }

    @Test
    public void getOrCreateTag_Should_CallRepositoryGet_When_TagTitleExists(){
        //Arrange
        User user = createMockUser();
        Tag tag = createMockTag();
        Mockito.when(mockTagRepository.getTagByTitle(tag.getTitle()))
                .thenReturn(tag);

        //Act
        tagService.getOrCreateTag(user, tag);

        //Assert
        Mockito.verify(mockTagRepository, Mockito.times(2))
                .getTagByTitle(tag.getTitle());
    }

    @Test
    public void updateTag_Should_ThrowException_When_UserBlocked(){
        //Arrange
        User user = createMockBlocked();
        Tag tag = createMockTag();

        //Act, Assert
        Assertions.assertThrows(AuthorizationException.class
                , ()->tagService.updateTag(user, tag));
    }

    @Test
    public void updateTag_Should_ThrowException_When_UserNotAdmin(){
        //Arrange
        User user = createMockUser();
        Tag tag = createMockTag();

        //Act, Assert
        Assertions.assertThrows(AuthorizationException.class
                , ()->tagService.updateTag(user, tag));
    }

    @Test
    public void updateTag_Should_ThrowException_When_TryingToCreateDuplicateTitle(){
        //Arrange
        User mockUser = createMockAdmin();
        Tag mockRepositoryTag = createMockTag();
        Tag newTag = createMockTag();
        newTag.setTagId(2);

        Mockito.when(mockTagRepository.getTagByTitle(Mockito.anyString())).thenReturn(mockRepositoryTag);

        //Act, Assert
        Assertions.assertThrows(EntityDuplicateException.class, ()-> tagService.updateTag(mockUser, newTag));
    }

    @Test
    public void updateTag_Should_CallRepository_When_UpdatingWithNewTitle(){
        //Arrange
        User mockUser = createMockAdmin();
        Tag newTag = createMockTag();

        Mockito.when(mockTagRepository.getTagByTitle(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        //Act
        tagService.updateTag(mockUser, newTag);
        //Assert
        Mockito.verify(mockTagRepository, Mockito.times(1)).updateTag(newTag);
    }

    @Test
    public void deleteTag_Should_ThrowException_When_UserBlocked(){
        //Arrange
        User user = createMockBlocked();
        Tag tag = createMockTag();

        //Act, Assert
        Assertions.assertThrows(AuthorizationException.class
                ,()-> tagService.deleteTag(user, tag.getTagId()));
    }

    @Test
    public void deleteTag_Should_ThrowException_When_UserIsNotAdmin(){
        //Arrange
        User user = createMockUser();
        Tag tag = createMockTag();

        //Act, Assert
        Assertions.assertThrows(AuthorizationException.class
                ,()-> tagService.deleteTag(user, tag.getTagId()));
    }

    @Test
    public void deleteTag_Should_CallPostRepository_When_PostsUseThisTag(){
        //Arrange
        User user = createMockAdmin();
        Tag tag = createMockTag();
        Post post = createMockPost();
        post.getTags().add(tag);
        List<Post> posts = new ArrayList<>();
        posts.add(post);

        Mockito.when(mockTagRepository.getTagById(Mockito.anyInt()))
                .thenReturn(tag);
        Mockito.when(mockPostRepository.getPosts())
                .thenReturn(posts);

        //Act
        tagService.deleteTag(user, tag.getTagId());
        // , Assert
        Mockito.verify(mockPostRepository, Mockito.times(1)).updatePost(post);
    }

    @Test
    public void deleteTag_Should_CallTagRepository(){
        //Arrange
        User user = createMockAdmin();
        Tag tag = createMockTag();
        Mockito.when(mockTagRepository.getTagById(Mockito.anyInt()))
                .thenReturn(tag);
        Mockito.when(mockPostRepository.getPosts())
                .thenReturn(new ArrayList<>());

        //Act
        tagService.deleteTag(user, tag.getTagId());
        // , Assert
        Mockito.verify(mockTagRepository, Mockito.times(1)).deleteTag(tag.getTagId());
    }

    @Test
    public void addTagToPost_Should_ThrowException_When_UserBlocked(){
        //Arrange
        User user = createMockBlocked();
        Post post = createMockPost();
        Tag tag = createMockTag();

        //Act, Assert
        Assertions.assertThrows(AuthorizationException.class
                ,()-> tagService.addTagToPost(user, post, tag));
    }

    @Test
    public void addTagToPost_Should_ThrowException_When_UserIsNotAdminOrCreator(){
        //Arrange
        User user = createMockUser();
        Post post = createMockPost();
        Tag tag = createMockTag();

        //Act, Assert
        Assertions.assertThrows(AuthorizationException.class
                ,()-> tagService.addTagToPost(Mockito.mock(User.class), post, tag));
    }

    @Test
    public void addTagToPost_Should_CallPostRepository_When_UserIsAdmin(){
        //Arrange
        User user = createMockAdmin();
        Post post = createMockPost();
        post.setTags(new HashSet<>());
        Tag tag = createMockTag();

        //Act
        tagService.addTagToPost(user, post, tag);
        // , Assert
        Mockito.verify(mockPostRepository, Mockito.times(1)).updatePost(post);
    }

    @Test
    public void addTagToPost_Should_CallPostRepository_When_UserIsCreator(){
        //Arrange
        Post post = createMockPost();
        User user = post.getUser();
        post.setTags(new HashSet<>());
        Tag tag = createMockTag();

        //Act
        tagService.addTagToPost(user, post, tag);
        // , Assert
        Mockito.verify(mockPostRepository, Mockito.times(1)).updatePost(post);
    }

    @Test
    public void deleteTagFromPost_Should_ThrowException_When_UserBlocked(){
        //Arrange
        User user = createMockBlocked();
        Post post = createMockPost();
        Tag tag = createMockTag();

        //Act, Assert
        Assertions.assertThrows(AuthorizationException.class
                ,()-> tagService.deleteTagFromPost(user, post, tag.getTagId()));
    }

    @Test
    public void deleteTagFromPost_Should_ThrowException_When_UserIsNotAdminOrCreator(){
        //Arrange
        Post post = createMockPost();
        Tag tag = createMockTag();

        //Act, Assert
        Assertions.assertThrows(AuthorizationException.class
                ,()-> tagService.deleteTagFromPost(Mockito.mock(User.class), post, tag.getTagId()));
    }

    @Test
    public void deleteTagFromPost_Should_ThrowException_When_TagDoesNotExist(){
        //Arrange
        User user = createMockAdmin();
        Post post = createMockPost();
        post.setTags(new HashSet<>());
        Tag tag = createMockTag();
        Mockito.when(mockTagRepository.getTagById(Mockito.anyInt()))
                .thenReturn(tag);

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class, ()->tagService.deleteTagFromPost(user, post, tag.getTagId()));
    }

    @Test
    public void deleteTagFromPost_Should_CallPostRepository_When_UserIsAdmin(){
        //Arrange
        User user = createMockAdmin();
        Post post = createMockPost();
        Tag tag = createMockTag();
        post.getTags().add(tag);
        Mockito.when(mockTagRepository.getTagById(Mockito.anyInt()))
                .thenReturn(tag);

        //Act
        tagService.deleteTagFromPost(user, post, tag.getTagId());
        // , Assert
        Mockito.verify(mockPostRepository, Mockito.times(1)).updatePost(post);
    }

    @Test
    public void deleteTagFromPost_Should_CallPostRepository_When_UserIsCreator(){
        //Arrange
        Post post = createMockPost();
        User user = post.getUser();
        Tag tag = createMockTag();
        post.getTags().add(tag);
        Mockito.when(mockTagRepository.getTagById(Mockito.anyInt()))
                .thenReturn(tag);

        //Act
        tagService.deleteTagFromPost(user, post, tag.getTagId());
        // , Assert
        Mockito.verify(mockPostRepository, Mockito.times(1)).updatePost(post);
    }
}
