package com.example.fitnessforum.services;

import com.example.fitnessforum.exceptions.AuthorizationException;
import com.example.fitnessforum.exceptions.EntityDuplicateException;
import com.example.fitnessforum.exceptions.EntityNotFoundException;
import com.example.fitnessforum.models.*;
import com.example.fitnessforum.repositories.contracts.CommentRepository;
import com.example.fitnessforum.repositories.contracts.LikeRepository;
import com.example.fitnessforum.repositories.contracts.PostRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.example.fitnessforum.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class PostServiceImplTests {
    @Mock
    PostRepository mockPostRepository;

    @Mock
    LikeRepository mockLikeRepository;

    @Mock
    CommentRepository mockCommentRepository;

    @InjectMocks
    PostServiceImpl postService;

    @Test
    public void getMostLikedPosts_Should_CallRepository(){
        //Arrange

        //Act
        postService.getMostLikedPosts();

        //Assert
        Mockito.verify(mockPostRepository, Mockito.times(1))
                .getMostLikedPosts();
    }

    @Test
    public void getPosts_Should_CallRepository() {
        // Arrange
        PostFilterOptions mockFilterOptions = createMockPostFilterOptions();
        Mockito.when(mockPostRepository.getPosts(mockFilterOptions)).thenReturn(null);
        // Act
        postService.getPosts(mockFilterOptions);

        // Assert
        Mockito.verify(mockPostRepository, Mockito.times(1))
                .getPosts(mockFilterOptions);
    }

    @Test
    public void getPostById_Should_CallRepository(){
        //Arrange

        //Act
        postService.getPostById(1);

        //Assert
        Mockito.verify(mockPostRepository, Mockito.times(1))
                .getPostById(1);
    }

    @Test
    public void getPostById_Should_ReturnPost_When_MatchByIdExists(){
        //Arrange
        Post mockRepositoryPost = createMockPost();
        Mockito.when(mockPostRepository.getPostById(Mockito.anyInt())).thenReturn(mockRepositoryPost);
        //Act
        Post post = postService.getPostById(1);

        //Assert
        Assertions.assertEquals(mockRepositoryPost, post);
    }

    @Test
    public void create_Should_ThrowException_When_UserBlocked(){
        //Arrange
        User mockUser = createMockBlocked();
        Post post = createMockPost();

        //Act, Assert
        Assertions.assertThrows(AuthorizationException.class, () -> postService.createPost(post, mockUser));
    }

    @Test
    public void create_Should_ThrowException_When_PostWithSameTitleExists(){
        //Arrange
        Post mockRepositoryPost = createMockPost();
        Post newPost = createMockPost();
        newPost.setPostId(2);
        newPost.setContent("new content");
        User mockUser = createMockUser();
        Mockito.when(mockPostRepository.getPostByTitle(Mockito.anyString())).thenReturn(mockRepositoryPost);

        //Act, Assert
        Assertions.assertThrows(EntityDuplicateException.class, () -> postService.createPost(newPost, mockUser));
    }

    @Test
    public void create_Should_CallRepository_When_PostWithSameTitleDoesNotExist(){
        //Arrange
        Post newPost = createMockPost();
        User mockUser = createMockUser();
        Mockito.when(mockPostRepository.getPostByTitle(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        //Act
        postService.createPost(newPost, mockUser);
        //Assert
        Mockito.verify(mockPostRepository, Mockito.times(1)).createPost(newPost);
    }

    @Test
    public void update_Should_ThrowException_When_UserBlocked(){
        //Arrange
        User mockUser = createMockUser();
        mockUser.setBlocked(true);
        Post post = createMockPost();

        //Act, Assert
        Assertions.assertThrows(AuthorizationException.class, () -> postService.updatePost(post, mockUser));
    }

    @Test
    public void update_Should_ThrowException_When_UserIsNotCreatorOrAdmin(){
        //Arrange
        Post post = createMockPost();
        Mockito.when(mockPostRepository.getPostById(Mockito.anyInt()))
                .thenReturn(post);

        //Act, Assert
        Assertions.assertThrows(AuthorizationException.class
                , () -> postService.updatePost(post, Mockito.mock(User.class)));
    }

    @Test
    public void update_Should_ThrowException_When_TryingToCreateDuplicateTitle(){
        //Arrange
        Post mockRepositoryPost = createMockPost();
        Post newPost = createMockPost();
        newPost.setPostId(2);
        User mockUser = createMockAdmin();
        Mockito.when(mockPostRepository.getPostById(Mockito.anyInt()))
                .thenReturn(mockRepositoryPost);
        Mockito.when(mockPostRepository.getPostByTitle(Mockito.anyString())).thenReturn(mockRepositoryPost);

        //Act, Assert
        Assertions.assertThrows(EntityDuplicateException.class, () -> postService.updatePost(newPost, mockUser));
    }

    @Test
    public void update_Should_CallRepository_When_UserIsCreator(){
        //Arrange
        Post mockPost = createMockPost();
        User creator = mockPost.getUser();
        Mockito.when(mockPostRepository.getPostById(Mockito.anyInt()))
                .thenReturn(mockPost);
        Mockito.when(mockPostRepository.getPostByTitle(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);
        //Act
        postService.updatePost(mockPost, creator);
        //Assert
        Mockito.verify(mockPostRepository, Mockito.times(1)).updatePost(mockPost);

    }

    @Test
    public void update_Should_CallRepository_When_UserIsAdmin(){
        //Arrange
        Post mockPost = createMockPost();
        User admin = createMockAdmin();
        Mockito.when(mockPostRepository.getPostById(Mockito.anyInt()))
                .thenReturn(mockPost);
        Mockito.when(mockPostRepository.getPostByTitle(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);
        //Act
        postService.updatePost(mockPost, admin);
        //Assert
        Mockito.verify(mockPostRepository, Mockito.times(1)).updatePost(mockPost);

    }

    @Test
    public void update_Should_CallRepository_When_UpdatingExistingPost(){
        //Arrange
        Post existingPost = createMockPost();
        User admin = createMockAdmin();
        Mockito.when(mockPostRepository.getPostById(Mockito.anyInt()))
                .thenReturn(existingPost);
        Mockito.when(mockPostRepository.getPostByTitle(Mockito.anyString()))
                .thenReturn(existingPost);
        //Act
        postService.updatePost(existingPost, admin);
        //Assert
        Mockito.verify(mockPostRepository, Mockito.times(1)).updatePost(existingPost);

    }

    @Test
    public void delete_Should_ThrowException_When_UserBlocked(){
        //Arrange
        Post post = createMockPost();
        User user = createMockBlocked();

        //Act, Assert
        Assertions.assertThrows(AuthorizationException.class, () -> postService.deletePost(post.getPostId(), user));
    }

    @Test
    public void delete_Should_ThrowException_When_UserIsNotCreatorOrAdmin(){
        //Arrange
        Post post = createMockPost();
        Mockito.when(mockPostRepository.getPostById(Mockito.anyInt()))
                .thenReturn(post);

        //Act, Assert
        Assertions.assertThrows(AuthorizationException.class
                , () -> postService.deletePost(post.getPostId(), Mockito.mock(User.class)));
    }

    @Test
    public void delete_Should_CallLikeRepository_When_LikesExist(){
        //Arrange
        Post post = createMockPost();
        Mockito.when(mockPostRepository.getPostById(Mockito.anyInt()))
                .thenReturn(post);
        User admin = createMockAdmin();

        Like like = createMockLike();
        List<Like> likes = new ArrayList<>();
        likes.add(like);
        Mockito.when(mockLikeRepository.getLikesByPostId(Mockito.anyInt()))
                .thenReturn(likes);

        //Act
        postService.deletePost(post.getPostId(), admin);

        //Assert
        Mockito.verify(mockLikeRepository, Mockito.times(1)).deleteLike(like.getLikeId());
    }

    @Test
    public void delete_Should_CallCommentRepository_When_CommentsExist(){
        //Arrange
        Post post = createMockPost();
        Mockito.when(mockPostRepository.getPostById(Mockito.anyInt()))
                .thenReturn(post);
        User admin = createMockAdmin();

        Comment comment = createMockComment();
        List<Comment> comments = new ArrayList<>();
        comments.add(comment);
        Mockito.when(mockCommentRepository.getCommentByPostId(Mockito.anyInt()))
                .thenReturn(comments);

        //Act
        postService.deletePost(post.getPostId(), admin);

        //Assert
        Mockito.verify(mockCommentRepository, Mockito.times(1)).deleteComment(comment.getCommentId());
    }

    @Test
    public void delete_Should_CallRepository_When_UserIsAdmin(){
        //Arrange
        Post post = createMockPost();
        Mockito.when(mockPostRepository.getPostById(Mockito.anyInt()))
                .thenReturn(post);
        User admin = createMockAdmin();

        //Act
        postService.deletePost(post.getPostId(), admin);

        //Assert
        Mockito.verify(mockPostRepository, Mockito.times(1)).deletePost(post.getPostId());
    }

    @Test
    public void delete_Should_CallRepository_When_UserIsCreator(){
        //Arrange
        Post post = createMockPost();
        Mockito.when(mockPostRepository.getPostById(Mockito.anyInt()))
                .thenReturn(post);
        User creator = post.getUser();

        //Act
        postService.deletePost(post.getPostId(), creator);

        //Assert
        Mockito.verify(mockPostRepository, Mockito.times(1)).deletePost(post.getPostId());
    }
}
