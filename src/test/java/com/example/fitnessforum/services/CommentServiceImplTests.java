package com.example.fitnessforum.services;

import com.example.fitnessforum.exceptions.AuthorizationException;
import com.example.fitnessforum.models.Comment;
import com.example.fitnessforum.models.Post;
import com.example.fitnessforum.models.User;
import com.example.fitnessforum.repositories.contracts.CommentRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.fitnessforum.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class CommentServiceImplTests {

    @Mock
    CommentRepository mockCommentRepository;

    @InjectMocks
    CommentServiceImpl commentService;

    @Test
    void getComments_Should_CallRepository() {
        // Arrange
        User user = createMockAdmin();
        Mockito.when(mockCommentRepository.getComments()).thenReturn(null);
        // Act
        commentService.getComments(user);
        // Assert
        Mockito.verify(mockCommentRepository, Mockito.times(1)).getComments();
    }

    @Test
    public void getComments_Should_ReturnComment_When_MatchByIdExist() {
        // Arrange
        Comment mockComment = createMockComment();
        User user = createMockAdmin();
        Mockito.when(mockCommentRepository.getCommentById(Mockito.anyInt()))
                .thenReturn(mockComment);
        // Act
        Comment result = commentService.getCommentById(mockComment.getCommentId(), user);

        // Assert
        Assertions.assertEquals(mockComment, result);
    }

    @Test
    void getWithFilter_Should_CallRepository() {
        // Arrange
        Post mockPost = createMockPost();

        // Act
        commentService.filter(mockPost.getPostId(), Optional.empty(),
                Optional.empty(), Optional.empty());

        // Assert
        Mockito.verify(mockCommentRepository, Mockito.times(1))
                .filter(mockPost.getPostId(), Optional.empty(),
                        Optional.empty(), Optional.empty());
    }


    @Test
    public void getComments_Should_ReturnComment_When_MatchByIdExistAndIsInPost() {
        // Arrange
        Comment mockComment = createMockComment();
        Mockito.when(mockCommentRepository.getCommentById(mockComment.getCommentId(),
                        mockComment.getPost().getPostId()))
                .thenReturn(mockComment);

        // Act
        Comment result = commentService.getCommentById(mockComment.getCommentId(),
                mockComment.getPost().getPostId());

        // Assert
        Assertions.assertEquals(mockComment, result);
    }

    @Test
    public void getCommentsByPostId_Should_ReturnComment_When_Match() {
        // Arrange
        List<Comment> mockCommentsList = new ArrayList<>();
        Comment mockComment = createMockComment();
        mockCommentsList.add(mockComment);
        Mockito.when(mockCommentRepository.getCommentByPostId(mockComment.getPost().getPostId()))
                .thenReturn(mockCommentsList);

        // Act
        List<Comment> result = commentService.getCommentByPostId(mockComment.getPost().getPostId());

        // Assert
        Assertions.assertEquals(mockCommentsList, result);
    }

    @Test
    public void create_Should_ThrowException_When_UserIsBlocked() {
        // Arrange
        Comment mockComment = createMockComment();
        User blocked = createMockBlocked();
        // Act, Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> commentService.createComment(mockComment,
                        blocked));
    }

    @Test
    public void create_Should_CallRepository_When_UserIsNotBlocked() {
        // Arrange
        Comment mockComment = createMockComment();
        User user = createMockUser();

        // Act
        commentService.createComment(mockComment, user);

        // Assert
        Mockito.verify(mockCommentRepository, Mockito.times(1))
                .createComment(mockComment);

    }

    @Test
    public void update_Should_ThrowException_When_UserIsNotCreatorOrAdmin() {
        // Arrange
        Comment mockComment = createMockComment();

        // Act, Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> commentService.updateComment(mockComment,
                        Mockito.mock(User.class), Mockito.mock(Post.class)));
    }

    @Test
    public void update_Should_ThrowException_When_UserIsBlocked() {
        // Arrange
        Comment mockComment = createMockComment();
        User blocked = createMockBlocked();
        // Act, Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> commentService.updateComment(mockComment,
                        blocked, Mockito.mock(Post.class)));
    }

    @Test
    public void update_Should_CallRepository_When_UserIsAdmin() {
        // Arrange
        Comment mockComment = createMockComment();
        User admin = createMockAdmin();

        // Act
        commentService.updateComment(mockComment, admin, Mockito.mock(Post.class));

        // Assert
        Mockito.verify(mockCommentRepository, Mockito.times(1))
                .updateComment(mockComment);

    }

    @Test
    public void update_Should_CallRepository_When_UserIsCreator() {
        // Arrange
        Comment mockComment = createMockComment();
        User creator = mockComment.getUser();

        // Act
        commentService.updateComment(mockComment, creator, Mockito.mock(Post.class));

        // Assert
        Mockito.verify(mockCommentRepository, Mockito.times(1))
                .updateComment(mockComment);

    }

    @Test
    public void updateOverloaded_Should_ThrowException_When_UserIsNotCreatorOrAdmin() {
        // Arrange
        Comment mockComment = createMockComment();

        // Act, Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> commentService.updateComment(mockComment,
                        Mockito.mock(User.class)));
    }

    @Test
    public void updateOverloaded_Should_ThrowException_When_UserIsBlocked() {
        // Arrange
        Comment mockComment = createMockComment();
        User blocked = createMockBlocked();
        // Act, Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> commentService.updateComment(mockComment,
                        blocked));
    }

    @Test
    public void updateOverloaded_Should_CallRepository_When_UserIsAdmin() {
        // Arrange
        Comment mockComment = createMockComment();
        User admin = createMockAdmin();

        // Act
        commentService.updateComment(mockComment, admin);

        // Assert
        Mockito.verify(mockCommentRepository, Mockito.times(1))
                .updateComment(mockComment);

    }

    @Test
    public void updateOverloaded_Should_CallRepository_When_UserIsCreator() {
        // Arrange
        Comment mockComment = createMockComment();
        User creator = mockComment.getUser();

        // Act
        commentService.updateComment(mockComment, creator);

        // Assert
        Mockito.verify(mockCommentRepository, Mockito.times(1))
                .updateComment(mockComment);

    }

    @Test
    void delete_Should_CallRepository_When_CommentUserIsCreator() {
        // Arrange
        Comment mockComment = createMockComment();
        User mockUserCreator = createMockUser();
        Post mockPost = createMockPost();

        Mockito.when(mockCommentRepository.getCommentById(Mockito.anyInt()))
                .thenReturn(mockComment);

        // Act
        commentService.deleteComment(1, mockUserCreator, mockPost);

        // Assert
        Mockito.verify(mockCommentRepository, Mockito.times(1))
                .deleteComment(1);
    }

    @Test
    void delete_Should_ThrowException_When_CommentCreatorIsBlocked() {
        // Arrange
        User mockBlockedUser = createMockBlocked();
        Post mockPost = createMockPost();

        // Act, Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> commentService.deleteComment(1, mockBlockedUser, mockPost));
    }

    @Test
    void delete_Should_CallRepository_When_UserIsAdmin() {
        // Arrange
        Comment mockComment = createMockComment();
        User mockUser = createMockAdmin();

        // Act
        commentService.deleteComment(mockComment.getCommentId(), mockUser);

        // Assert
        Mockito.verify(mockCommentRepository, Mockito.times(1))
                .deleteComment(mockComment.getCommentId());
    }

    @Test
    void delete_Should_ThrowException_When_UserIsNotAdmin() {
        // Arrange
        Comment comment = createMockComment();
        //User mockUser = createMockUser();
        Mockito.when(mockCommentRepository.getCommentById(Mockito.anyInt()))
                .thenReturn(comment);

        // Act, Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> commentService.deleteComment(1, Mockito.mock(User.class)));
    }
}
