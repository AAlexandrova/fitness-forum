package com.example.fitnessforum.services;

import com.example.fitnessforum.exceptions.AuthorizationException;
import com.example.fitnessforum.exceptions.CustomException;
import com.example.fitnessforum.exceptions.EntityDuplicateException;
import com.example.fitnessforum.exceptions.EntityNotFoundException;
import com.example.fitnessforum.models.Phone;
import com.example.fitnessforum.models.User;
import com.example.fitnessforum.repositories.contracts.PhoneRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.example.fitnessforum.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class PhoneServiceImplTests {

    @Mock
    PhoneRepository mockPhoneRepository;

    @InjectMocks
    PhoneServiceImpl phoneService;

    @Test
    void getPhones_Should_CallRepository() {
        // Arrange
        Mockito.when(mockPhoneRepository.getPhones()).thenReturn(null);

        // Act
        phoneService.getPhones();
        
        // Assert
        Mockito.verify(mockPhoneRepository, Mockito.times(1))
                .getPhones();
    }

    @Test
    public void getPhones_Should_ReturnPhone_When_MatchByIdExist() {
        // Arrange
        Phone mockPhone = createMockPhone();

        Mockito.when(mockPhoneRepository.getPhoneById(Mockito.anyInt()))
                .thenReturn(mockPhone);

        // Act
        Phone result = phoneService.getPhoneById(mockPhone.getPhoneId());

        // Assert
        Assertions.assertEquals(mockPhone, result);
    }

    @Test
    void getWithFilter_Should_CallRepository() {
        // Arrange
        Mockito.when(mockPhoneRepository.filter(Optional.empty(), Optional.empty(), Optional.empty()))
                .thenReturn(null);

        // Act
        phoneService.filter(Optional.empty(), Optional.empty(), Optional.empty());

        // Assert
        Mockito.verify(mockPhoneRepository, Mockito.times(1))
                .filter(Optional.empty(), Optional.empty(), Optional.empty());
    }

    @Test
    public void getPhones_Should_ReturnPhone_When_MatchByUserIdExist() {
        // Arrange
        Phone mockPhone = createMockPhone();

        Mockito.when(mockPhoneRepository.getPhoneByUserId(mockPhone.getUser().getUserId()))
                .thenReturn(mockPhone);

        // Act
        Phone result = phoneService.getPhoneByUserId(mockPhone.getUser().getUserId());

        // Assert
        Assertions.assertEquals(mockPhone, result);
    }

    @Test
    public void create_Should_CallRepository_When_PhoneWithSameIdDoesNotExist() {
        // Arrange
        Phone mockPhone = createMockPhone();
        User mockUser = createMockAdmin();

        Mockito.when(mockPhoneRepository.getPhoneById(mockPhone.getPhoneId()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        phoneService.createPhone(mockPhone, mockUser);

        // Assert
        Mockito.verify(mockPhoneRepository, Mockito.times(1))
                .createPhone(mockPhone);
    }

    @Test
    public void create_Should_Throw_When_PhoneWithSameNameExists() {
        // Arrange
        Phone mockPhone = createMockPhone();
        User mockUser = createMockAdmin();
        Mockito.when(mockPhoneRepository.getPhoneById(mockPhone.getPhoneId()))
                .thenReturn(mockPhone);

        // Act, Assert
        Assertions.assertThrows(
                EntityDuplicateException.class,
                () -> phoneService.createPhone(mockPhone, mockUser));
    }

    @Test
    public void create_Should_Throw_When_UserOfThePhoneIsNotAdmin() {
        // Arrange
        Phone mockPhone = createMockPhone();
        mockPhone.setUser(createMockUser());
        User mockUser = createMockAdmin();

        // Act, Assert
        Assertions.assertThrows(
                CustomException.class,
                () -> phoneService.createPhone(mockPhone, mockUser));
    }

    @Test
    void update_Should_CallRepository_When_PhoneCreatorIsAdmin() {
        // Arrange
        Phone mockPhone = createMockPhone();
        User mockUser = createMockAdmin();

        Mockito.when(mockPhoneRepository.getPhoneById(Mockito.anyInt()))
                .thenReturn(mockPhone);

        // Act
        phoneService.updatePhone(mockPhone, mockUser);

        // Assert
        Mockito.verify(mockPhoneRepository, Mockito.times(1))
                .updatePhone(mockPhone);
    }

    @Test
    void update_Should_CallRepository_When_PhoneExist() {
        // Arrange
        Phone mockPhone = createMockPhone();
        User mockUserAdmin = createMockAdmin();

        Mockito.when(mockPhoneRepository.getPhoneById(Mockito.anyInt()))
                .thenReturn(mockPhone);

        Mockito.when(mockPhoneRepository.getPhoneById(Mockito.anyInt()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        phoneService.updatePhone(mockPhone, mockUserAdmin);

        // Assert
        Mockito.verify(mockPhoneRepository, Mockito.times(1))
                .updatePhone(mockPhone);
    }

    @Test
    public void update_Should_CallRepository_When_UpdatingExistingPhone() {
        // Arrange
        Phone mockPhone = createMockPhone();
        User mockUser = createMockAdmin();

        Mockito.when(mockPhoneRepository.getPhoneById(Mockito.anyInt()))
                .thenReturn(mockPhone);

        // Act
        phoneService.updatePhone(mockPhone, mockUser);

        // Assert
        Mockito.verify(mockPhoneRepository, Mockito.times(1))
                .updatePhone(mockPhone);
    }

    @Test
    public void update_Should_ThrowException_When_PhoneCreatorIsNotAdmin() {
        // Arrange
        Phone mockPhone = createMockPhone();

        // Act, Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> phoneService.updatePhone(mockPhone, Mockito.mock(User.class)));
    }

    @Test
    public void update_Should_ThrowException_When_PhoneIsTaken() {
        // Arrange
        Phone mockPhone = createMockPhone();
        User mockUser = createMockAdmin();

        Mockito.when(mockPhoneRepository.getPhoneById(Mockito.anyInt()))
                .thenReturn(mockPhone);

        Phone mockExistingPhoneWithSameNumber = createMockPhone();
        mockExistingPhoneWithSameNumber.setPhoneId(2);

        Mockito.when(mockPhoneRepository.getPhoneById(mockUser.getUserId()))
                .thenReturn(mockExistingPhoneWithSameNumber);

        // Act, Assert
        Assertions.assertThrows(
                EntityDuplicateException.class,
                () -> phoneService.updatePhone(mockPhone, mockUser));
    }

    @Test
    void delete_Should_CallRepository_When_PhoneCreatorIsAdmin() {
        // Arrange
        User mockUser = createMockAdmin();

        // Act
        phoneService.deletePhone(1, mockUser);

        // Assert
        Mockito.verify(mockPhoneRepository, Mockito.times(1))
                .deletePhone(1);
    }

    @Test
    void delete_Should_ThrowException_When_PhoneCreatorIsNotBlocked() {
        // Arrange
        User mockUserNotBlocked = createMockBlocked();

        // Act, Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> phoneService.deletePhone(1, mockUserNotBlocked));
    }
}
