# Forum System

## Project Description

A forum system that allows users to create posts, add comments, and upvote/downvote the things they like or dislike. The topic of the forum is fitness.

## Functional Requirements

### Entities

#### Users

-   Must have first and last name, email, username, and password.
    -   First name and last name must be between 4 and 32 symbols.
    -   Email must be a valid email and unique in the system.
    -   Username is also unique in the system.

#### Admins

-   Must have first and last name, email, and may have a phone number.
    -   First name and last name must be between 4 and 32 symbols.
    -   Email must be a valid email and unique in the system.

#### Posts

-   Must have a user who created it, a title, content, comments and the number of likes it has received.
    -   The title must be between 16 and 64 symbols.
    -   The content must be between 32 symbols and 8192 symbols.
    -   Other users must be able to post replies.

### Public Part

-   Accessible without authentication
-   On the home page, anonymous users are presented with the core features of the platform as well as how many people are using it and how many posts have been created so far.
-   Anonymous users can register and log in.
-   Anonymous users can see a list of the top 10 most commented posts and a list of the 10 most recently created posts.

### Private Part

-   Accessible only if the user is authenticated.
-   The user can log in and log out.
-   Users can browse posts created by other users with an option to sort and filter them.
-   Users can view a single post, including its title, content, comments, likes, etc. The details of the post and any available user actions (comment/like/edit) are presented on the same page.
-   Users can update their profile information.
-   Users can create a new post with at least a title and content.
-   Each user can edit only their own posts or comments.
-   Each user can view all their or any other user's posts and comments (with the option to filter and sort them).
-   Each user can remove one or more of their own posts.
-   Each user can comment/reply to any other forum post.
-   Each user can like or dislike the posts of other users.

### Administrative Part

-   Accessible to users with administrative privileges.
-   Admins can search for a user by their username, email, first name.
-   Admins can block or unblock individual users. A blocked user cannot create posts, comments, likes or tags.
-   Admins can updtae and delete any post and comment.
-   Admins can view a list of all posts with an option to filter and sort them.

### Optional Feature: Post Tags

-   A tag is additional information that can be added to each post after creation.
-   The user can add tags to his/her own post after viewing the details of the individual post. If the tag does not exist, a new one must be added to the database. If the tag exists, the existing one in the database must be reused. All tags should be lowercase only.
-   Other users can find posts by typing in the tag in the search bar.
-   Users can add, remove, and edit tags on their own posts. 
-   Admins are able to add, remove, and edit tags on all posts.

### REST API

To provide other developers access to the service, a REST API is developed. The REST API uses HTTP as a transport protocol and clear text JSON for request and response payloads.

The API is documented with Swagger:

http://localhost:8080/swagger-ui/index.html#/


The REST API provides the following capabilities:

1.  Users
    -   CRUD operations
    -   Search by first name, last name, username, email, admin, blocked

2.  Admin
    -   Make other users admin
    -   Block/unblock user
    -   Delete user
    -   Delete posts
    -   CRUD operations for phones

3.  Posts
    -   CRUD operations
    -   Like/Dislike
    -   Sort and filter posts
    -   Filter all posts by tag

4.  Comments
    -   CRUD operations

5.  Tags (admin functionality)
    -   CRUD operations

## Conclusion

These are the functional requirements and features that our forum system has. Enjoy using it!


 





