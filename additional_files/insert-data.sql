-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия на сървъра:            10.5.18-MariaDB - mariadb.org binary distribution
-- ОС на сървъра:                Win64
-- HeidiSQL Версия:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Дъмп на структурата на БД fitness_forum
CREATE DATABASE IF NOT EXISTS `fitness_forum` /*!40100 DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci */;
USE `fitness_forum`;

-- Дъмп структура за таблица fitness_forum.comments
CREATE TABLE IF NOT EXISTS `comments` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  PRIMARY KEY (`comment_id`),
  KEY `comments_posts_fk` (`post_id`),
  KEY `comments_users_fk` (`user_id`),
  CONSTRAINT `comments_posts_fk` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`),
  CONSTRAINT `comments_users_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- Дъмп данни за таблица fitness_forum.comments: ~10 rows (приблизително)
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` (`comment_id`, `content`, `user_id`, `post_id`) VALUES
	(1, 'At age 35 need to include two strength-training sessions each week.', 4, 6),
	(2, 'Strength training is a key component of fitness', 7, 6),
	(3, 'Exercise after age 35 can require a different approach', 6, 6),
	(4, 'Bench press mistakes', 5, 1),
	(5, 'What you think is the reason?', 2, 1),
	(6, 'I can give you advice.', 4, 1),
	(7, 'Food supplements', 5, 5),
	(8, 'Pills, tablets, capsules, liquids in measured doses', 2, 5),
	(9, 'What about proteins?', 7, 5),
	(10, 'Proteins are among the most abundant organic molecules', 4, 5);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- Дъмп структура за таблица fitness_forum.likes
CREATE TABLE IF NOT EXISTS `likes` (
  `like_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `up_or_down` tinyint(1) NOT NULL,
  PRIMARY KEY (`like_id`),
  KEY `likes_posts_fk` (`post_id`),
  KEY `likes_users_fk` (`user_id`),
  CONSTRAINT `likes_posts_fk` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`),
  CONSTRAINT `likes_users_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- Дъмп данни за таблица fitness_forum.likes: ~10 rows (приблизително)
/*!40000 ALTER TABLE `likes` DISABLE KEYS */;
INSERT INTO `likes` (`like_id`, `user_id`, `post_id`, `up_or_down`) VALUES
	(1, 2, 2, 1),
	(2, 9, 5, 1),
	(3, 1, 6, 0),
	(4, 7, 2, 1),
	(5, 7, 4, 1),
	(6, 7, 5, 1),
	(7, 5, 2, 0),
	(8, 1, 1, 1),
	(9, 2, 3, 1),
	(10, 6, 3, 1);
/*!40000 ALTER TABLE `likes` ENABLE KEYS */;

-- Дъмп структура за таблица fitness_forum.phones
CREATE TABLE IF NOT EXISTS `phones` (
  `phone_id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(15) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`phone_id`),
  UNIQUE KEY `phones_pk` (`phone`),
  KEY `phones_users_fk` (`user_id`),
  CONSTRAINT `phones_users_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- Дъмп данни за таблица fitness_forum.phones: ~2 rows (приблизително)
/*!40000 ALTER TABLE `phones` DISABLE KEYS */;
INSERT INTO `phones` (`phone_id`, `phone`, `user_id`) VALUES
	(1, '0887888888', 9),
	(2, '0887333333', 10);
/*!40000 ALTER TABLE `phones` ENABLE KEYS */;

-- Дъмп структура за таблица fitness_forum.posts
CREATE TABLE IF NOT EXISTS `posts` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) NOT NULL,
  `content` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `data_created` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`post_id`),
  KEY `posts_users_fk` (`user_id`),
  CONSTRAINT `posts_users_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- Дъмп данни за таблица fitness_forum.posts: ~7 rows (приблизително)
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` (`post_id`, `title`, `content`, `user_id`, `data_created`) VALUES
	(1, 'Bench press problem', 'My shoulder hurts when I bench press.', 1, '2023-02-26 13:05:38'),
	(2, 'Program for beginner', 'Please send me a fitness program for a beginner.', 4, '2023-02-26 13:06:45'),
	(3, 'Training for marathon', 'I want to run a marathon. Please advise how I should train.', 3, '2023-02-26 13:07:15'),
	(4, 'Training for fat loss', 'I want to lose fat. Please advise how I should train.', 2, '2023-02-26 13:07:47'),
	(5, 'Supplements', 'Supplement info, feedback and reviews!', 1, '2023-02-26 13:08:55'),
	(6, 'Over Age 35', 'With age comes experience!', 8, '2023-02-26 13:09:57'),
	(7, 'Workout Equipment', 'All about equipment and iron!', 8, '2023-02-26 13:10:58');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- Дъмп структура за таблица fitness_forum.posts_tags
CREATE TABLE IF NOT EXISTS `posts_tags` (
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  KEY `posts_tags_posts_fk` (`post_id`),
  KEY `posts_tags_tags_fk` (`tag_id`),
  CONSTRAINT `posts_tags_posts_fk` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`),
  CONSTRAINT `posts_tags_tags_fk` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- Дъмп данни за таблица fitness_forum.posts_tags: ~11 rows (приблизително)
/*!40000 ALTER TABLE `posts_tags` DISABLE KEYS */;
INSERT INTO `posts_tags` (`post_id`, `tag_id`) VALUES
	(1, 6),
	(1, 6),
	(6, 1),
	(6, 5),
	(2, 3),
	(2, 2),
	(5, 7),
	(4, 1),
	(4, 4),
	(3, 4),
	(3, 2);
/*!40000 ALTER TABLE `posts_tags` ENABLE KEYS */;

-- Дъмп структура за таблица fitness_forum.tags
CREATE TABLE IF NOT EXISTS `tags` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) NOT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- Дъмп данни за таблица fitness_forum.tags: ~7 rows (приблизително)
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` (`tag_id`, `title`) VALUES
	(1, 'problems'),
	(2, 'program'),
	(3, 'beginner'),
	(4, 'training'),
	(5, 'age'),
	(6, 'press'),
	(7, 'supplements');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;

-- Дъмп структура за таблица fitness_forum.users
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(32) NOT NULL,
  `last_name` varchar(32) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `is_admin` tinyint(1) NOT NULL,
  `is_blocked` tinyint(1) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `users_pk` (`username`),
  UNIQUE KEY `users_pk2` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- Дъмп данни за таблица fitness_forum.users: ~10 rows (приблизително)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `email`, `username`, `password`, `is_admin`, `is_blocked`) VALUES
	(1, 'Ivan', 'Kostadinov', 'kostadinov@abv.bg', 'ivan', 'pass1', 0, 0),
	(2, 'Georgi', 'Kostov', 'kostov@gmail.com', 'georgi', 'pass2', 0, 0),
	(3, 'Miava', 'Denkova', 'denkova@yahoo.com', 'miava', 'pass3', 0, 0),
	(4, 'David', 'Ivov', 'ivov@abv.bg', 'david', 'pass4', 0, 0),
	(5, 'Misho', 'Antov', 'antov@abv.bg', 'misho', 'pass5', 0, 0),
	(6, 'Reneta', 'Didova', 'didova@yahoo.com', 'reneta', 'pass6', 0, 0),
	(7, 'Merian', 'Krysteva', 'krysteva@gmail.com', 'merian', 'pass7', 0, 0),
	(8, 'Kiril', 'Adminov', 'adminov@abv.bg', 'kiril', 'pass8', 0, 0),
	(9, 'Boris', 'Elenkov', 'elenkov@abv.bg', 'boris', 'pass9', 1, 0),
	(10, 'Albena', 'Petkova', 'petkova@gmail.com', 'albena', 'pass10', 1, 0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
