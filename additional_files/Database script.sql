create or replace table tags
(
    tag_id int auto_increment
        primary key,
    title  varchar(64) not null
);

create or replace table users
(
    user_id    int auto_increment
        primary key,
    first_name varchar(32)  not null,
    last_name  varchar(32)  not null,
    email      varchar(100) not null,
    username   varchar(32)  not null,
    password   varchar(32)  not null,
    is_admin   tinyint(1)   not null,
    is_blocked tinyint(1)   not null,
    constraint users_pk
        unique (username),
    constraint users_pk2
        unique (email)
);

create or replace table phones
(
    phone_id int auto_increment
        primary key,
    phone    varchar(15) not null,
    user_id  int         not null,
    constraint phones_pk
        unique (phone),
    constraint phones_users_fk
        foreign key (user_id) references users (user_id)
);

create or replace table posts
(
    post_id      int auto_increment
        primary key,
    title        varchar(64)                           not null,
    content      text                                  not null,
    user_id      int                                   not null,
    data_created timestamp default current_timestamp() not null,
    constraint posts_users_fk
        foreign key (user_id) references users (user_id)
);

create or replace table comments
(
    comment_id int auto_increment
        primary key,
    content    text not null,
    user_id    int  not null,
    post_id    int  not null,
    constraint comments_posts_fk
        foreign key (post_id) references posts (post_id),
    constraint comments_users_fk
        foreign key (user_id) references users (user_id)
);

create or replace table likes
(
    like_id    int auto_increment
        primary key,
    user_id    int        not null,
    post_id    int        not null,
    up_or_down tinyint(1) not null,
    constraint likes_posts_fk
        foreign key (post_id) references posts (post_id),
    constraint likes_users_fk
        foreign key (user_id) references users (user_id)
);

create or replace table posts_tags
(
    post_id int not null,
    tag_id  int not null,
    constraint posts_tags_posts_fk
        foreign key (post_id) references posts (post_id),
    constraint posts_tags_tags_fk
        foreign key (tag_id) references tags (tag_id)
);

